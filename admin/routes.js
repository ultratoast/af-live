"use strict"
var isBrowser=new Function("try {return this===window;}catch(e){ return false;}")
if (!isBrowser()) {
	require('babel-register')({
	  presets: ['es2015','react']
	}) //fix es6 + jsx templates for require
}

const DefaultLayout = require('./admin.jsx'),
	DefaultLayoutHeadless = require('./views/default.jsx'),
	Welcome = require('./views/welcome.jsx'),
	Blog = require('./containers/blog.js').default,
	AddBlogPost = require('./containers/addblogpost.js').default,
	EditBlogPost = require('./containers/editblogpost.js').default,
	Forum = require('./containers/forum.js').default,
	AddForumPost = require('./containers/addforumpost.js').default,
	EditForumPost = require('./containers/editforumpost.js').default,
	User = require('./containers/users.js').default,
	Products = require('./containers/products.js').default,
	AddProduct = require('./containers/addproduct.js').default,
	EditProduct = require('./containers/editproduct.js').default

const RouteConfig = [{
	path: 'admin',
	indexRoute: {component: Welcome},
	component: DefaultLayoutHeadless,
	childRoutes: [
		{path: 'blog', component: Blog},
		{path: 'blog/add', component: AddBlogPost},
		{path: 'blog/edit/*', component: EditBlogPost},
		{path: 'forum', component: Forum},
		{path: 'forum/add', component: AddForumPost},
		{path: 'forum/edit/*', component: EditForumPost},
		{path: 'users', component: User},
		{path: 'products', component: Products},
		{path: 'products/add', component: AddProduct},
		{path: 'products/edit/*', component: EditProduct}
	]
}]

if (!isBrowser()) {
	RouteConfig[0].path = '/'
	RouteConfig[0].component = DefaultLayout
}

module.exports = RouteConfig