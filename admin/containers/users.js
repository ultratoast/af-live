import { connect } from 'react-redux'
import UsersIndex from '../views/users.jsx' 

const mapStateToProps = (state) => {
	return {
		users: state.users || []
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const GetUsers = connect(
	mapStateToProps
	// mapDispatchToProps
)(UsersIndex)

export default GetUsers