import { connect } from 'react-redux'
import AddPostIndex from '../views/addforumpost.jsx' 

const mapStateToProps = (state) => {
	return {
		forum: state.forum || [],
		session: state.session || []
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const AddForumPost = connect(
	mapStateToProps
	// mapDispatchToProps
)(AddPostIndex)

export default AddForumPost