import { connect } from 'react-redux'
import AddProductIndex from '../views/addproduct.jsx' 

const mapStateToProps = (state) => {
	return {
		products: state.products
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const AddProduct = connect(
	mapStateToProps
	// mapDispatchToProps
)(AddProductIndex)

export default AddProduct