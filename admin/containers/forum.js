import { connect } from 'react-redux'
import ForumIndex from '../views/forum.jsx' 

const mapStateToProps = (state) => {
	return {
		forum: state.forum,
		session: state.session
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const GetForum = connect(
	mapStateToProps
	// mapDispatchToProps
)(ForumIndex)

export default GetForum