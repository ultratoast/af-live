import { connect } from 'react-redux'
import EditPostIndex from '../views/editforumpost.jsx' 

const mapStateToProps = (state) => {
	return {
		forum: state.forum
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const EditForumPost = connect(
	mapStateToProps
	// mapDispatchToProps
)(EditPostIndex)

export default EditForumPost