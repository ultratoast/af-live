import { connect } from 'react-redux'
import BlogIndex from '../views/blog.jsx' 

const mapStateToProps = (state) => {
	return {
		blog: state.blog
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const GetBlog = connect(
	mapStateToProps
	// mapDispatchToProps
)(BlogIndex)

export default GetBlog