import { connect } from 'react-redux'
import AddPostIndex from '../views/addblogpost.jsx' 

const mapStateToProps = (state) => {
	return {
		blog: state.blog
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const AddBlogPost = connect(
	mapStateToProps
	// mapDispatchToProps
)(AddPostIndex)

export default AddBlogPost