import { connect } from 'react-redux'
import ProductsIndex from '../views/products.jsx' 

const mapStateToProps = (state) => {
	return {
		products: state.products
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const GetProducts = connect(
	mapStateToProps
	// mapDispatchToProps
)(ProductsIndex)

export default GetProducts