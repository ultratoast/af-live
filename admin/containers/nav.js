import { connect } from 'react-redux'
import Nav from '../views/nav.jsx' 

const mapStateToProps = (state) => {
	return {
		session: state.session || [],
		users: state.users || []
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const GetNav = connect(
	mapStateToProps
	// mapDispatchToProps
)(Nav)

export default GetNav