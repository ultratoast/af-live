import { connect } from 'react-redux'
import EditProductIndex from '../views/editproduct.jsx' 

const mapStateToProps = (state) => {
	return {
		products: state.products
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const EditProduct = connect(
	mapStateToProps
	// mapDispatchToProps
)(EditProductIndex)

export default EditProduct