import { connect } from 'react-redux'
import EditPostIndex from '../views/editblogpost.jsx' 

const mapStateToProps = (state) => {
	return {
		blog: state.blog
	}
}

// const mapDispatchToProps = (dispatch) => {
// 	return {

// 	}
// }

const EditBlogPost = connect(
	mapStateToProps
	// mapDispatchToProps
)(EditPostIndex)

export default EditBlogPost