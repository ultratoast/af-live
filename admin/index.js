const React = require('react'),
	ReactDOM = require('react-dom'),
	Router = require('react-router/lib/Router'),
	Provider = require('react-redux/lib/components/Provider'),
	browserHistory = require('react-router/lib/browserHistory'),
	match = require('react-router/lib/match'),
	routes = require('./routes'),
	createStore = require('redux/lib/createStore').default,
	applyMiddleware = require('redux/lib/applyMiddleware').default,
	thunk = require('redux-thunk').default,
	mainReducer = require('./main-reducer').default,
	content = document.getElementById('content')

match({ routes:routes, history:browserHistory }, (error, redirectLocation, renderProps) => {
	let initialStateElem = document.getElementById('initialState'),
		stateString = initialStateElem.dataset.state,
		stateJson = JSON.parse(stateString),
    	store = applyMiddleware(thunk)(createStore)(mainReducer, stateJson), 
    	provider = React.createFactory(Provider.default),
		router = React.createFactory(Router),
		component = provider({store: store},
			router(renderProps)
		)
	initialStateElem.dataset.state = []
	ReactDOM.render(component,content)

})