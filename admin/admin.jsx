const React = require('react'),
	Nav = require('./containers/nav').default

const AdminPage = React.createClass({
	render: function() {
		let data = JSON.stringify(this.props.params)
		return (
			<html>
				<head>
					<title>Audiofusion Admin App</title>
					<link rel="stylesheet" href="/css/adminstyles.css"></link>
				</head>
				<body>
					<div id="content">
						<section className="main" id="admin">
							<Nav/>
							{this.props.children}
						</section>
					</div>
					<script id="initialState" data-state={data}></script>
					<script src="/js/admin.bundle.js"></script>
				</body>
			</html>
		)
	}
})

module.exports = AdminPage