require('es6-promise').polyfill()
import request from 'superagent'

const config = require('../../config')

export const REQUEST_PRODUCTS = 'REQUEST_PRODUCTS'

export const REQUEST_PRODUCTS_SUCCESS = 'REQUEST_PRODUCTS_SUCCESS'

export const REQUEST_PRODUCTS_FAILURE = 'REQUEST_PRODUCTS_FAILURE'

export const REQUEST_ONE_PRODUCT = 'REQUEST_ONE_PRODUCT'

export const REQUEST_ONE_PRODUCT_SUCCESS = 'REQUEST_ONE_PRODUCT_SUCCESS'

export const REQUEST_ONE_PRODUCT_FAILURE = 'REQUEST_ONE_PRODUCT_FAILURE'

export const ADD_PRODUCT = 'ADD_PRODUCT'

export const ADD_PRODUCT_SUCCESS = 'ADD_PRODUCT_SUCCESS'

export const ADD_PRODUCT_FAILURE = 'ADD_PRODUCT_FAILURE'

export const EDIT_PRODUCT = 'EDIT_PRODUCT'

export const EDIT_PRODUCT_SUCCESS = 'EDIT_PRODUCT_SUCCESS'

export const EDIT_PRODUCT_FAILURE = 'EDIT_PRODUCT_FAILURE'

export const DELETE_PRODUCT = 'DELETE_PRODUCT'

export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS'

export const DELETE_PRODUCT_FAILURE = 'DELETE_PRODUCT_FAILURE'


export function addStoreProduct(product) {
	return { type: ADD_PRODUCT, product }
}

export function editStoreProduct(product) {
	return { type: EDIT_PRODUCT, product }
}

export function deleteStoreProduct(product) {
	return { type: DELETE_PRODUCT, product }
}

function requestProductsFailure() {
	return { type: REQUEST_PRODUCTS_FAILURE, receivedAt: Date.now() }
}

function requestProducts(products) {
	return { type: REQUEST_PRODUCTS }
}

function requestProductsSuccess(json) {
	return {
		type: REQUEST_PRODUCTS_SUCCESS,
		products: json.Category.products,
		receivedAt: Date.now()
	}
}

export function getProducts() {
	return dispatch => {
		dispatch(requestProducts())
		return request
			.get('https://'+config.host+'/api/products/category')
			.set('Content-Type', 'application/json')
			.accept('json')
			.on('error',(err) => {
				dispatch(requestProductsFailure())
				console.log('error',err)
			})
			.then(response => dispatch(requestProductsSuccess(response.body)))
	}
}

function shouldGetProducts(state) {
	let products = state.products
	if (!products || products.length == 0) {
		return true
	} else if (products.isFetching) {
		return false
	} else {
		return products.didInvalidate
	}
}

export function getProductsIfNeeded() {
	return (dispatch, getState) => {
		if (shouldGetProducts(getState())) {
			return dispatch(getProducts())
		}
	}
}