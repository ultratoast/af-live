require('es6-promise').polyfill()
import request from 'superagent'

const config = require('../../config')

export const REQUEST_BLOG_POSTS = 'REQUEST_BLOG_POSTS'

export const REQUEST_BLOG_POSTS_SUCCESS = 'REQUEST_BLOG_POSTS_SUCCESS'

export const REQUEST_BLOG_POSTS_FAILURE = 'REQUEST_BLOG_POSTS_FAILURE'

export const REQUEST_ALL_BLOG_POSTS = 'REQUEST_ALL_BLOG_POSTS'

export const REQUEST_ALL_BLOG_POSTS_SUCCESS = 'REQUEST_ALL_BLOG_POSTS_SUCCESS'

export const REQUEST_ALL_BLOG_POSTS_FAILURE = 'REQUEST_ALL_BLOG_POSTS_FAILURE'

export const REQUEST_ONE_BLOG_POST = 'REQUEST_ONE_BLOG_POST'

export const REQUEST_ONE_BLOG_POST_SUCCESS = 'REQUEST_ONE_BLOG_POST_SUCCESS'

export const REQUEST_ONE_BLOG_POST_FAILURE = 'REQUEST_ONE_BLOG_POST_FAILURE'

export const ADD_BLOG_POST = 'ADD_BLOG_POST'

export const ADD_BLOG_POST_SUCCESS = 'ADD_BLOG_POST_SUCCESS'

export const ADD_BLOG_POST_FAILURE = 'ADD_BLOG_POST_FAILURE'

export const EDIT_BLOG_POST = 'EDIT_BLOG_POST'

export const EDIT_BLOG_POST_SUCCESS = 'EDIT_BLOG_POST_SUCCESS'

export const EDIT_BLOG_POST_FAILURE = 'EDIT_BLOG_POST_FAILURE'

export const DELETE_BLOG_POST = 'DELETE_BLOG_POST'

export const DELETE_BLOG_POST_SUCCESS = 'DELETE_BLOG_POST_SUCCESS'

export const DELETE_BLOG_POST_FAILURE = 'DELETE_BLOG_POST_FAILURE'

export const UPVOTE_BLOG_POST = 'UPVOTE_BLOG_POST'

export const UPVOTE_BLOG_POST_SUCCESS = 'UPVOTE_BLOG_POST_SUCCESS'

export const UPVOTE_BLOG_POST_FAILURE = 'UPVOTE_BLOG_POST_FAILURE'

export const ADD_BLOG_POST_REPLY = 'ADD_BLOG_POST_REPLY'

export const ADD_BLOG_POST_REPLY_SUCCESS = 'ADD_BLOG_POST_REPLY_SUCCESS'

export const ADD_BLOG_POST_REPLY_FAILURE = 'ADD_BLOG_POST_REPLY_FAILURE'

export const DELETE_BLOG_POST_REPLY = 'DELETE_BLOG_POST_REPLY'

export const DELETE_BLOG_POST_REPLY_SUCCESS = 'DELETE_BLOG_POST_REPLY_SUCCESS'

export const DELETE_BLOG_POST_REPLY_FAILURE = 'DELETE_BLOG_POST_REPLY_FAILURE'

export const UPVOTE_BLOG_POST_REPLY = 'UPVOTE_BLOG_POST_REPLY'

export const UPVOTE_BLOG_POST_REPLY_SUCCESS = 'UPVOTE_BLOG_POST_REPLY_SUCCESS'

export const UPVOTE_BLOG_POST_REPLY_FAILURE = 'UPVOTE_BLOG_POST_REPLY_FAILURE'

export function addBlogPost(post) {
	return { type: ADD_BLOG_POST, post }
}

export function editBlogPost(post) {
	return { type: EDIT_BLOG_POST, post }
}

export function deleteBlogPost(post) {
	return { type: DELETE_BLOG_POST, post }
}

function requestBlogPostsFailure() {
	return { type: REQUEST_BLOG_POSTS_FAILURE, receivedAt: Date.now() }
}

function requestBlogPosts() {
	return { type: REQUEST_BLOG_POSTS }
}

function requestBlogPostsSuccess(json) {
	return {
		type: REQUEST_BLOG_POSTS_SUCCESS,
		blog: json.GetLatestPosts.posts,
		receivedAt: Date.now()
	}
}

function getBlogPosts() {
	return dispatch => {
		dispatch(requestBlogPosts())
		return request
			.get('https://'+config.host+'/api/blog/latest')
			.set('Content-Type', 'application/json')
			.accept('json')
			.on('error',(err) => {
				dispatch(requestBlogPostsFailure())
				console.log('error',err)
			})
			.then(response => dispatch(requestBlogPostsSuccess(response.body)))
	}
}

function shouldGetBlogPosts(state) {
	let blog = state.blog
	if (!blog || blog.length == 0) {
		return true
	} else if (blog.isFetching) {
		return false
	} else {
		return blog.didInvalidate
	}
}

export function getBlogPostsIfNeeded() {
	return (dispatch,getState) => {
		if (shouldGetBlogPosts(getState())) {
			return dispatch(getBlogPosts())
		}
	}
}