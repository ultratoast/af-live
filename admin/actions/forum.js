require('es6-promise').polyfill()
import request from 'superagent'

const config = require('../../config')

export const REQUEST_FORUM_POSTS = 'REQUEST_FORUM_POSTS'

export const REQUEST_FORUM_POSTS_SUCCESS = 'REQUEST_FORUM_POSTS_SUCCESS'

export const REQUEST_FORUM_POSTS_FAILURE = 'REQUEST_FORUM_POSTS_FAILURE'

export const REQUEST_ALL_FORUM_POSTS = 'REQUEST_ALL_FORUM_POSTS'

export const REQUEST_ALL_FORUM_POSTS_SUCCESS = 'REQUEST_ALL_FORUM_POSTS_SUCCESS'

export const REQUEST_ALL_FORUM_POSTS_FAILURE = 'REQUEST_ALL_FORUM_POSTS_FAILURE'

export const REQUEST_ONE_FORUM_POST = 'REQUEST_ONE_FORUM_POST'

export const REQUEST_ONE_FORUM_POST_SUCCESS = 'REQUEST_ONE_FORUM_POST_SUCCESS'

export const REQUEST_ONE_FORUM_POST_FAILURE = 'REQUEST_ONE_FORUM_POST_FAILURE'

export const ADD_FORUM_POST = 'ADD_FORUM_POST'

export const ADD_FORUM_POST_SUCCESS = 'ADD_FORUM_POST_SUCCESS'

export const ADD_FORUM_POST_FAILURE = 'ADD_FORUM_POST_FAILURE'

export const EDIT_FORUM_POST = 'EDIT_FORUM_POST'

export const EDIT_FORUM_POST_SUCCESS = 'EDIT_FORUM_POST_SUCCESS'

export const EDIT_FORUM_POST_FAILURE = 'EDIT_FORUM_POST_FAILURE'

export const DELETE_FORUM_POST = 'DELETE_FORUM_POST'

export const DELETE_FORUM_POST_SUCCESS = 'DELETE_FORUM_POST_SUCCESS'

export const DELETE_FORUM_POST_FAILURE = 'DELETE_FORUM_POST_FAILURE'

export const UPVOTE_FORUM_POST = 'UPVOTE_FORUM_POST'

export const UPVOTE_FORUM_POST_SUCCESS = 'UPVOTE_FORUM_POST_SUCCESS'

export const UPVOTE_FORUM_POST_FAILURE = 'UPVOTE_FORUM_POST_FAILURE'

export const ADD_FORUM_POST_REPLY = 'ADD_FORUM_POST_REPLY'

export const ADD_FORUM_POST_REPLY_SUCCESS = 'ADD_FORUM_POST_REPLY_SUCCESS'

export const ADD_FORUM_POST_REPLY_FAILURE = 'ADD_FORUM_POST_REPLY_FAILURE'

export const DELETE_FORUM_POST_REPLY = 'DELETE_FORUM_POST_REPLY'

export const DELETE_FORUM_POST_REPLY_SUCCESS = 'DELETE_FORUM_POST_REPLY_SUCCESS'

export const DELETE_FORUM_POST_REPLY_FAILURE = 'DELETE_FORUM_POST_REPLY_FAILURE'

export const UPVOTE_FORUM_POST_REPLY = 'UPVOTE_FORUM_POST_REPLY'

export const UPVOTE_FORUM_POST_REPLY_SUCCESS = 'UPVOTE_FORUM_POST_REPLY_SUCCESS'

export const UPVOTE_FORUM_POST_REPLY_FAILURE = 'UPVOTE_FORUM_POST_REPLY_FAILURE'

function upvoteForumPostReplyFailure() {
	return { type: UPVOTE_FORUM_POST_REPLY_FAILURE }
}

function upvoteForumPostReplySuccess(json) {
	return {
		type: UPVOTE_FORUM_POST_REPLY_SUCCESS,
		receivedAt: Date.now()
	}
}

function upvoteForumPostReplyAction() {
	return { type: UPVOTE_FORUM_POST_REPLY }
}

export function upvoteForumPostReply(reply,user) {
	return dispatch => {
		dispatch(upvoteForumPostReplyAction())
		return request
			.post('https://'+config.host+'/api/forum/'+reply+'/upvote')
			.set('Content-Type', 'application/json')
			.accept('json')
			.send({'user':user})
			.on('error',(err) => {
				dispatch(upvoteForumPostReplyFailure())
				console.log('error',err)
			})
			.then(response => dispatch(upvoteForumPostReplySuccess(response.body)))
	}
}

function deleteForumPostReplyFailure() {
	return { type: DELETE_FORUM_POST_REPLY_FAILURE }
}

function deleteForumPostReplySuccess(json) {
	return {
		type: DELETE_FORUM_POST_REPLY_SUCCESS,
		receivedAt: Date.now()
	}
}

function deleteForumPostReplyAction() {
	return { type: DELETE_FORUM_POST_REPLY }
}

export function deleteForumPostReply(post,reply) {
	return dispatch => {
		dispatch(deleteForumPostReplyAction())
		return request 
			.post('https://'+config.host+'/api/forum/'+post+'/reply/'+reply)
			.set('Content-Type', 'application/json')
			.accept('json')
			.on('error',(err) => {
				dispatch(deleteForumPostReplyFailure())
				console.log('error',err)
			})
			.then(response => dispatch(deleteForumPostReplySuccess(response.body)))
	}
}

function addForumPostReplyFailure() {
	return { type: ADD_FORUM_POST_REPLY_FAILURE }
}

function addForumPostReplySuccess(json) {
	return {
		type: ADD_FORUM_POST_REPLY_SUCCESS,
		receivedAt: Date.now()
	}
}

function addForumPostReplyAction() {
	return { type: ADD_FORUM_POST_REPLY }
}

export function addForumPostReply(post,reply) {
	return dispatch => {
		dispatch(addForumPostReplyAction())
		return request
			.post('https://'+config.host+'/api/forum/'+post+'/reply')
			.set('Content-Type', 'application/json')
			.accept('json')
			.send({'reply':reply})
			.on('error',(err) => {
				dispatch(addForumPostReplyFailure())
				console.log('error',err)
			})
			.then(response => dispatch(addForumPostReplySuccess(response.body)))
	}
}

function upvoteForumPostFailure() {
	return { type: UPVOTE_FORUM_POST_FAILURE }
}

function upvoteForumPostSuccess(json) {
	return {
		type: UPVOTE_FORUM_POST_SUCCESS,
		receivedAt: Date.now()
	}
}

function upvoteForumPostAction() {
	return { type: UPVOTE_FORUM_POST }
}

export function upvoteForumPost(post,user) {
	return dispatch => {
		dispatch(upvoteForumPostAction())
		return request
			.post('https://'+config.host+'/api/forum/'+post+'/upvote')
			.set('Content-Type', 'application/json')
			.accept('json')
			.send({'user':user})
			.on('error',(err) => {
				dispatch(upvoteForumPostFailure())
				console.log('error',err)
			})
			.then(response => dispatch(upvoteForumPostSuccess(response.body)))
	}
}

function addForumPostSuccess(json,getState) {
	let state = getState(),
		posts = state.forum.posts.items
	posts.push(json.AddPost.post)
	return {
		type: ADD_FORUM_POST_SUCCESS,
		posts: posts,
		receivedAt: Date.now()
	}
}

function addForumPostFailure() {
	return {
		type: ADD_FORUM_POST_FAILURE,
		receivedAt: Date.now()
	}
}

function addForumPostAction() {
	return { type: ADD_FORUM_POST }
}

export function addForumPost(post,token) {
	return (dispatch,getState) => {
		dispatch(addForumPostAction())
		return request
			.post('https://'+config.host+'/api/forum/add')
			.set('Content-Type', 'application/json')
			.set('Authorization','Bearer '+token)
			.accept('json')
			.send({'post':post})
			.on('error',(err) => {
				dispatch(addForumPostFailure())
				console.log('error',err)
			})
			.then(response => dispatch(addForumPostSuccess(response.body,getState)))
	}
}

function editForumPostSuccess(json) {
	return {
		type: EDIT_FORUM_POST_SUCCESS,
		receivedAt: Date.now()
	}
}

function editForumPostFailure() {
	return {
		type: EDIT_FORUM_POST_FAILURE,
		receivedAt: Date.now()
	}
}

function editForumPostAction() {
	return { type: EDIT_FORUM_POST }
}

export function editForumPost(post) {
	return dispatch => {
		dispatch(editForumPostAction())
		return request
			.post('https://'+config.host+'/api/forum/'+post.id+'/edit')
			.set('Content-Type', 'application/json')
			.accept('json')
			.send({'post':post})
			.on('error',(err) => {
				dispatch(editForumPostFailure())
				console.log('error',err)
			})
			.then(response => dispatch(editForumPostSuccess(response.body)))
	}
}

function deleteForumPostSuccess(json) {
	return {
		type: DELETE_FORUM_POST_SUCCESS,
		receivedAt: Date.now()
	}
}

function deleteForumPostFailure() {
	return {
		type: DELETE_FORUM_POST_FAILURE,
		receivedAt: Date.now()
	}
}

function deleteForumPostAction() {
	return { type: DELETE_FORUM_POST }
}

export function deleteForumPost(post) {
	return dispatch => {
		dispatch(deleteForumPostAction())
		return request
			.del('https://'+config.host+'/api/forum/'+post+'/delete')
			.set('Content-Type', 'application/json')
			.accept('json')
			.on('error',(err) => {
				dispatch(deleteForumPostFailure())
				console.log('error',err)
			})
			.then(response => dispatch(deleteForumPostSuccess(response.body)))
	}
}

function requestOneForumPostFailure() {
	return { type: REQUEST_ONE_FORUM_POST_FAILURE, receivedAt: Date.now() }
}

function requestOneForumPostSuccess() {
	return {
		type: REQUEST_ONE_FORUM_POST_SUCCESS,
		post: json.GetPost.post,
		receivedAt: Date.now()		
	}
}

function requestOneForumPost() {
	return { type: REQUEST_ONE_FORUM_POST }
}

export function getOneForumPost(post) {
	return dispatch => {
		dispatch(requestOneForumPost())
		return request
			.get('https://'+config.host+'/api/fourm/'+post)
			.set('Content-Type', 'application/json')
			.accept('json')
			.on('error',(err) => {
				dispatch(requestOneForumPostFailure())
				console.log('error',err)
			})
			.then(response => dispatch(requestOneForumPostSuccess(response.body)))
	}
}

function requestAllForumPostsFailure() {
	return { type: REQUEST_ALL_FORUM_POSTS_FAILURE, receivedAt: Date.now() }
}

function requestAllForumPosts() {
	return { type: REQUEST_ALL_FORUM_POSTS }
}

function requestAllForumPostsSuccess(json) {
	return {
		type: REQUEST_ALL_FORUM_POSTS_SUCCESS,
		forum: json.GetAllPosts.posts,
		receivedAt: Date.now()
	}
}

export function getAllForumPosts() {
	return dispatch => {
		dispatch(requestAllForumPosts())
		return request
			.get('https://'+config.host+'/api/forum/all')
			.set('Content-Type', 'application/json')
			.accept('json')
			.on('error',(err) => {
				dispatch(requestAllForumPostsFailure())
				console.log('error',err)
			})
			.then(response => dispatch(requestAllForumPostsSuccess(response.body)))
	}
}

function requestForumPostsFailure() {
	return { type: REQUEST_FORUM_POSTS_FAILURE, receivedAt: Date.now() }
}

function requestForumPosts() {
	return { type: REQUEST_FORUM_POSTS }
}

function requestForumPostsSuccess(json) {
	return {
		type: REQUEST_FORUM_POSTS_SUCCESS,
		forum: json.GetLatestPosts.posts,
		receivedAt: Date.now()
	}
}

export function getForumPosts() {
	return dispatch => {
		dispatch(requestForumPosts())
		return request
			.get('https://'+config.host+'/api/forum/latest')
			.set('Content-Type', 'application/json')
			.accept('json')
			.on('error',(err) => {
				dispatch(requestForumPostsFailure())
				console.log('error',err)
			})
			.then(response => dispatch(requestForumPostsSuccess(response.body)))
	}
}

function shouldGetForumPosts(state) {
	let forum = state.forum
	if (!forum || forum.length == 0) {
		return true
	} else if (forum.isFetching) {
		return false
	} else {
		return forum.didInvalidate
	}
}

export function getForumPostsIfNeeded() {
	return (dispatch, getState) => {
		if (shouldGetForumPosts(getState())) {
			return dispatch(getForumPosts())
		}
	}
}