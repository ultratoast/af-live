require('es6-promise').polyfill()
import request from 'superagent'

const config = require('../../config')

export const REQUEST_SESSION = 'REQUEST_SESSION'

export const REQUEST_SESSION_SUCCESS = 'REQUEST_SESSION_SUCCESS'

export const REQUEST_SESSION_FAILURE = 'REQUEST_SESSION_FAILURE'

export const DELETE_SESSION = 'DELETE_SESSION'

export const DELETE_SESSION_SUCCESS = 'DELETE_SESSION_SUCCESS'

export const DELETE_SESSION_FAILURE = 'DELETE_SESSION_FAILURE'

function deleteSessionSuccess(json) {
	return {
		type: DELETE_SESSION_SUCCESS,
		receivedAt: Date.now(),
		session: json.Logout.session
	}
}

function deleteSessionFailure() {
	return { type: DELETE_SESSION_FAILURE, receivedAt: Date.now() }
}

function deleteSession() {
	return { type: DELETE_SESSION }
}

function stopSession(state) {
	return dispatch => {
		dispatch(deleteSession())
		return request
			.post('https://'+config.host+'/api/users/logout')
			.set('Content-Type', 'application/json')
			.set('Authorization','Bearer '+state.session.session.items.token)
			.accept('json')
			.send({'sessionid':state.session.session.items.id})
			.on('Error', (err) => {
				dispatch(deleteSessionFailure())
				console.log('Error',err)
			})
			.then(response => dispatch(deleteSessionSuccess(response.body)))
	}
}

function shouldStopSession(state) {
	let session = state.session
	if (session || session.length > 0) {
		return true
	} else if (session.isFetching) {
		return false
	} else {
		return session.didInvalidate
	}
}

export function stopSessionIfNeeded() {
	return (dispatch,getState) => {
		let state = getState()
		if (shouldStopSession(state)) {
			return dispatch(stopSession(state))
		}
	}
}

function requestSessionSuccess(json) {
	return {
		type: REQUEST_SESSION_SUCCESS,
		session: json.Login.session,
		receivedAt: Date.now()
	}
}

function requestSessionFailure() {
	return { type: REQUEST_SESSION_FAILURE, receivedAt: Date.now() }
}

function requestSession() {
	return { type: REQUEST_SESSION }
}

export function startSession(credentials) {
	return dispatch => {
		dispatch(requestSession())
		return request
			.post('https://'+config.host+'/api/users/login')
			.set('Content-Type', 'application/json')
			.accept('json')
			.send({'credentials':credentials})
			.on('error',(err) => {
				dispatch(requestSessionFailure())
				console.log('error',err)
			})
			.then(response => dispatch(requestSessionSuccess(response.body)))
	}
}

// function shouldGetSession(state) {
// 	let session = state.session
// 	if (!session || session.length == 0 || session.session.items.user == 'undefined') {
// 		return true
// 	} else if (session.isFetching) {
// 		return false
// 	} else {
// 		return session.didInvalidate
// 	}
// }

// export function getSessionIfNeeded(credentials) {
// 	return (dispatch,getState) => {
// 		if (shouldGetSession(getState())) {
// 			return dispatch(startSession(credentials))
// 		}
// 	}
// }