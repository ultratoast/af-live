require('es6-promise').polyfill()
import request from 'superagent'

const config = require('../../config')

export const REQUEST_USERS = 'REQUEST_USERS'

export const REQUEST_USERS_SUCCESS = 'REQUEST_USERS_SUCCESS'

export const REQUEST_USERS_FAILURE = 'REQUEST_USERS_FAILURE'

export const REQUEST_ONE_USER = 'REQUEST_ONE_USER'

export const REQUEST_ONE_USER_SUCCESS = 'REQUEST_ONE_USER_SUCCESS'

export const REQUEST_ONE_USER_FAILURE = 'REQUEST_ONE_USER_FAILURE'

export const ADD_USER = 'ADD_USER'

export const ADD_USER_SUCCESS = 'ADD_USER_SUCCESS'

export const ADD_USER_FAILURE = 'ADD_USER_FAILURE'

export const EDIT_USER = 'EDIT_USER'

export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS'

export const EDIT_USER_FAILURE = 'EDIT_USER_FAILURE'

export const DELETE_USER = 'DELETE_USER'

export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS'

export const DELETE_USER_FAILURE = 'DELETE_USER_FAILURE'

export function addUser(user) {
	return { type: ADD_USER, user }
}

export function editUser(user) {
	return { type: EDIT_USER, user }
}

export function deleteUser(user) {
	return { type: DELETE_USER, user }
}

function requestUsersFailure() {
	return { type: REQUEST_USERS_FAILURE, receivedAt: Date.now() }
}

function requestUsers(users) {
	return { type: REQUEST_USERS }
}

function requestUsersSuccess(json) {
	return {
		type: REQUEST_USERS_SUCCESS,
		users: json.GetAllUsers.users,
		receivedAt: Date.now()
	}
}

export function getUsers(state) {
	return dispatch => {
		console.log('state',state)
		dispatch(requestUsers())
		return request
			.get('https://'+config.host+'/api/users/all')
			.set('Content-Type', 'application/json')
			.set('Authorization', 'Bearer '+state.session.session.items.token)
			.accept('json')
			.on('error',(err) => {
				dispatch(requestUsersFailure())
				console.log('error',err)
			})
			.then(response => dispatch(requestUsersSuccess(response.body)))
	}
}

function shouldGetUsers(state) {
	let users = state.users
	if (!users || users.length == 0) {
		return true
	} else if (users.isFetching) {
		return false
	} else {
		return users.didInvalidate
	}
}

export function getUsersIfNeeded() {
	return (dispatch, getState) => {
		let state = getState()
		if (shouldGetUsers(state)) {
			return dispatch(getUsers(state))
		}
	}
}
