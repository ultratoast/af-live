require('es6-promise').polyfill()

const React = require('react'),
	Link = require('react-router/lib/Link'),
	sessionActions = require('../actions/session'),
	userActions = require('../actions/users')

var userstate, dispatch

const Nav = React.createClass({
	getInitialState: function() {
		return {
			credentials: {
				email: null,
				password: null,
			}
		}
	},
	componentDidMount: function() {
		dispatch = this.props.dispatch
	},	
	startSession: function() {
		if (!this.refs.email.value || !this.refs.password.value) {
			return false
		}
		this.setState({credentials: {email: this.refs.email.value,password:this.refs.password.value}}, () => {
			Promise.all([dispatch(sessionActions.startSession(this.state.credentials))])
			.then((response) => {
				console.log('session started',response)
				this.setState({credentials: {email: null,password:null}})
				dispatch(userActions.getUsersIfNeeded())
			})
			.catch((err) => {
				console.log('session error',err)
				this.refs.email.value = ''
				this.refs.password.value = ''
			})
		})
	},
	stopSession: function() {
		Promise.all([dispatch(sessionActions.stopSessionIfNeeded())])
		.then((response) => {
			console.log('session deleted',response)
		})
		.catch((err) => {
			console.log('session error',err)
		})
	},
	render: function() {		
		let sessionData = this.props.session.session ? this.props.session.session.items.user : false,
			title = sessionData ? sessionData.firstName : 'Admin'
		userstate = sessionData ? <div>Welcome { title || 'Admin' }<button onClick={this.stopSession}>Log Out</button></div> : <div><input ref="email" type="email" placeholder="Enter Email"/><input ref="password" type="password" placeholder="Enter Password"/><button onClick={this.startSession}>Log In</button></div> 
		return (
			<ul className="nav">
				<li><Link to="/admin">Home</Link></li>
				<li><Link to="/admin/blog">Blog</Link></li>
				<li><Link to="/admin/forum">Forum</Link></li>
				<li><Link to="/admin/users">Users</Link></li>
				<li><Link to="/admin/products">Products</Link></li>
				<li>{ userstate }</li>
			</ul>
		)
	}
})

module.exports = Nav