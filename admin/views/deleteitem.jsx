const React = require('react'),
	blog = require('../actions/blog'),
	forum = require('../actions/forum'),
	product = require('../actions/products'),
	user = require('../actions/users')

const DeleteItem = React.createClass({
	handleClick: function() {
		let { dispatch, type, item } = this.props
		switch (type) {
			case "user":
				dispatch(actions.deleteUser(item))
				break;
			case "product":
				dispatch(actions.deleteProduct(item))
				break;
			case "forum":
				dispatch(actions.deleteForumPost(item))
				break;
			case "blog":
				dispatch(actions.deleteBlogPost(item))
				break;
		}
	},
	render: function() {
		return (
			<button onClick={this.handleClick}>Delete</button>
		)
	}
})

module.exports = DeleteItem