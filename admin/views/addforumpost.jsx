const React = require('react'),
	ReactQuill = require('react-quill'),
	actions = require('../actions/forum'),
	CategoryList = require('./categorylist.jsx')

const AddForumPost = React.createClass({
  	contextTypes: {
    	router: React.PropTypes.object.isRequired
  	},
	getInitialState: function() {
		return {postContent: '', categories: []}
	},
	componentWillMount: function() {
		if (!this.props.session.session) {
			this.context.router.replace('/admin/forum')
		}
	},
	savePost: function() {
		let { dispatch } = this.props,
			author = this.props.session.session.items.user._id,
			title = this.refs.title.value,
			content = this.state.postContent,
			isPublic = this.refs.isPublic.checked,
			categories = this.state.categories.join(','),
			type = "forum",
			addPostObj = {
				title,
				author,
				content,
				isPublic,
				categories,
				type
			},
			token = this.props.session.session.items.token
		Promise.all([dispatch(actions.addForumPost(addPostObj, token))])
		.then((response) => {
			this.refs.title.value = ''
			this.refs.quilljs.value = ''
			this.refs.isPublic.value = false
			this.setState({ categories: []})
		})
		.catch((err) => {
			console.log('Add Post Error',err)
		})
	},
	onTextChange: function(value) {
		this.setState({postContent: value})
	},
	addCategory: function() {
		let categories = this.state.categories
		categories.push(this.refs.category.value)
		this.setState({categories:categories})
		this.refs.category.value = ''
	},
	render: function() {
		return (
			<div>
				<h2>Add Post</h2>
				<input type="text" ref="title" placeholder="Title"/>
				<ReactQuill ref="quilljs" theme="snow" onChange={this.onTextChange}>
					<ReactQuill.Toolbar key="toolbar"
                            ref="toolbar"
                            items={ReactQuill.Toolbar.defaultItems} />
                    <div key="editor"
             			ref="editor"
             			className="quill-contents"/>
				</ReactQuill>
				<input ref="category" name="category" type="text" placeholder="Enter a Category"/>
				<button onClick={this.addCategory}>Add Category</button>
				<br />
				<CategoryList categories={this.state.categories}/>
				<label htmlFor="isPublic">Make post public?</label>
				<input type="checkbox" ref="isPublic" name="isPublic"/>
				<button onClick={this.savePost}>Save Post</button>
			</div>
		)
	}
})

module.exports = AddForumPost