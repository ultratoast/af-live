const React = require('react')

const Product = React.createClass({
	render: function(){
		let product = this.props.product
		return (
			<li>{product}</li>
		)
	}
})

module.exports = Product