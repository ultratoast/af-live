const React = require('react'),
	Nav = require('../containers/nav').default

const Default = React.createClass({
	render: function()  {
		return (
			<section className="main" id="admin">
				<Nav />
				{this.props.children}
			</section>
		)
	}
})

module.exports = Default