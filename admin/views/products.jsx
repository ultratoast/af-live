const React = require('react'),
	actions = require('../actions/products'),
	Product = require('./product.jsx')

const ProductsIndex = React.createClass({
	componentDidMount: function() {
	},
	render: function() {
		let products = this.props.products.products.items || [],
			productList = products.length > 0 ? <ul>+products.map((product,i)=>{mapProduct(product,i)})+</ul> : <p>No products found</p>,
			mapProduct = (product,i) => {
				return <Product key={i} product={product}/>
			}	
		return (		
			<div>
				<p>Store Admin</p>
				{productList}
			</div>
		)
	}
})

module.exports = ProductsIndex