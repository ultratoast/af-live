const React = require('react'),
	actions = require('../actions/forum'),
	Post = require('./post.jsx'),
	Link = require('react-router/lib/Link')

const ForumIndex = React.createClass({
	componentDidMount: function() {
	},
	render: function() {
		let posts = this.props.forum.posts.items || [],
			forum = posts.length > 0 ? <ul>{posts.map((post,i)=>{return <Post key={i} post={post} type="forum"/>})}</ul> : <p>No posts found</p>,
			addLink = this.props.session.session ? <Link to="/admin/forum/add">Add Post</Link> : false
		return (
			<div>
				<h2>Forum Admin</h2>
				{ addLink }
				{ forum }
			</div>
		)
	}
})

module.exports = ForumIndex