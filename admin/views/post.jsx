const React = require('react'),
	Link = require('react-router/lib/Link'),
	DeleteItem = require('./deleteitem.jsx')

const Post = React.createClass({
	render: function() {
		let post = this.props.post
		return (
			<li className="post">
				<strong>{post.title}</strong> | <span>{ post.upvotes } Upvotes</span>  | <span>{ post.isPublic ? "Published" : "Draft" }</span>
				<br />
				<span>By: { post.author }</span> | <span>Posted: {post.postDate.toString()}</span>
				<br />
				<div className="preview" dangerouslySetInnerHTML={ {__html: post.content} }></div>
				<Link to="/admin/{this.props.type}/edit/{post._id}">Edit</Link>
				<DeleteItem type="this.props.type" item="post._id"/>
			</li>
		)
	}
})

module.exports = Post

