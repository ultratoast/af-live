const React = require('react')

Array.prototype.remove = function(value) {
    if (this.indexOf(value)!==-1) {
       this.splice(this.indexOf(value), 1);
       return true;
   } else {
      return false;
   };
} 

const CategoryList = React.createClass({
	getInitialState: function() {
		return { categories: this.props.categories}
	},
	deleteCategory: function(index) {
		let categories = this.state.categories
		categories.remove(index.target.textContent)
		this.setState({categories:categories})
	},
	render: function() {
		let categoryList = this.state.categories,
			categories = categoryList.length > 0 ? categoryList.map((category,i) => {return <li key={i} onClick={this.deleteCategory}>{category}</li>}) : false
	
		return (
			<ul className="categories">
				{categories}
			</ul>
		)
	}
})

module.exports = CategoryList