const React = require('react'),
	actions = require('../actions/blog'),
	Post = require('./post.jsx')

const BlogIndex = React.createClass({
	componentDidMount: function() {
	},
	render: function() {
		let posts = this.props.blog.posts.items || [],
			blog = posts.length > 0 ? <ul>+posts.map((post,i)=>{mapPost(post,i)})+</ul> : <p>No posts found</p>,
			mapPost = (post,i) => {
				let isPublic = post.isPublic ? <Post key={i} post={post}/> : null
				return isPublic
			}
		return (
			<div>
				<h2>Blog Admin</h2>
				{blog}
			</div>
		)
	}
})

module.exports = BlogIndex