const React = require('react'),
	actions = require('../actions/users'),
	User = require('./user.jsx')

const UsersIndex = React.createClass({
	componentDidMount: function() {
	},
	render: function() {
		let users = this.props.users.length > 0 ? this.props.users.users.items : [],
			userList = users.length > 0 ? <ul>+users.map((user,i)=>{mapUser(user,i)})+</ul> : <p>No users found</p>,
			mapUser = (user,i) => {
				return <User key={i} user={user}/>
			}
		return (
			<div>
				<h2>User Admin</h2>
				{userList}
			</div>
		)
	}
})

module.exports = UsersIndex