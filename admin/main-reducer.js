import { combineReducers  } from 'redux'

const blog = require('./reducers/blog').default, 
	forum = require('./reducers/forum').default,
	users = require('./reducers/users').default,
	products = require('./reducers/products').default,
	session = require('./reducers/session').default

const mainReducer = combineReducers({
	blog,
	forum,
	users,
	products,
	session
})

export default mainReducer