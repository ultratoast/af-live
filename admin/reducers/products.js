import {
	REQUEST_PRODUCTS,
	REQUEST_PRODUCTS_SUCCESS,
	REQUEST_PRODUCTS_FAILURE,
	REQUEST_ONE_PRODUCT,
	REQUEST_ONE_PRODUCT_SUCCESS,
	REQUEST_ONE_PRODUCT_FAILURE,
	ADD_PRODUCT,
	ADD_PRODUCT_SUCCESS,
	ADD_PRODUCT_FAILURE,
	EDIT_PRODUCT,
	EDIT_PRODUCT_SUCCESS,
	EDIT_PRODUCT_FAILURE,
	DELETE_PRODUCT,
	DELETE_PRODUCT_SUCCESS,
	DELETE_PRODUCT_FAILURE
} from '../actions/products'

const initialState = []

function getproducts(state = {
	isFetching: false,
	didInvalidate: false,
	items: []
}, action) {
	let actionObject = {
		REQUEST_PRODUCTS: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		REQUEST_PRODUCTS_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			items: action.products,
			lastUpdated: action.receivedAt
		}),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']
}

function handleProductsActions(state, action) {
	return Object.assign({}, state, {
		products: getproducts(state.products, action)
	})
}
function products(state, action) {
	if (typeof state === "undefined") {
		return initialState
	}
	let actionObject = {
		REQUEST_PRODUCTS: handleProductsActions(state, action),
		REQUEST_PRODUCTS_SUCCESS: handleProductsActions(state, action),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']	
}

export default products