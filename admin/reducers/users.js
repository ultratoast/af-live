import {
	REQUEST_USERS,
	REQUEST_USERS_SUCCESS,
	REQUEST_USERS_FAILURE,
	REQUEST_ONE_USER,
	REQUEST_ONE_USER_SUCCESS,
	REQUEST_ONE_USER_FAILURE,
	ADD_USER,
	ADD_USER_SUCCESS,
	ADD_USER_FAILURE,
	EDIT_USER,
	EDIT_USER_SUCCESS,
	EDIT_USER_FAILURE,
	DELETE_USER,
	DELETE_USER_SUCCESS,
	DELETE_USER_FAILURE
} from '../actions/users'

const initialState = []


function getusers(state = {
	isFetching: false,
	didInvalidate: false,
	items: []
}, action) {
	let actionObject = {
		REQUEST_USERS: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		REQUEST_USERS_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			items: action.users,
			lastUpdated: action.receivedAt
		}),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']
}

function handleUsersActions(state, action) {
	return Object.assign({}, state, {
		users: getusers(state.users, action)
	})
}
function users(state, action) {
	if (typeof state === "undefined") {
		return initialState
	}
	let actionObject = {
		REQUEST_USERS: handleUsersActions(state, action),
		REQUEST_USERS_SUCCESS: handleUsersActions(state, action),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']	
}

export default users