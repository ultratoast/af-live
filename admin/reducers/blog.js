import {
	REQUEST_BLOG_POSTS,
	REQUEST_BLOG_POSTS_SUCCESS,
	REQUEST_BLOG_POSTS_FAILURE,
	REQUEST_ALL_BLOG_POSTS,
	REQUEST_ALL_BLOG_POSTS_SUCCESS,
	REQUEST_ALL_BLOG_POSTS_FAILURE,
	REQUEST_ONE_BLOG_POST,
	REQUEST_ONE_BLOG_POST_SUCCESS,
	REQUEST_ONE_BLOG_POST_FAILURE,
	ADD_BLOG_POST,
	ADD_BLOG_POST_SUCCESS,
	ADD_BLOG_POST_FAILURE,
	EDIT_BLOG_POST,
	EDIT_BLOG_POST_SUCCESS,
	EDIT_BLOG_POST_FAILURE,
	DELETE_BLOG_POST,
	DELETE_BLOG_POST_SUCCESS,
	DELETE_BLOG_POST_FAILURE,
	UPVOTE_BLOG_POST,
	UPVOTE_BLOG_POST_SUCCESS,
	UPVOTE_BLOG_POST_FAILURE,
	ADD_BLOG_POST_REPLY,
	ADD_BLOG_POST_REPLY_SUCCESS,
	ADD_BLOG_POST_REPLY_FAILURE,
	DELETE_BLOG_POST_REPLY,
	DELETE_BLOG_POST_REPLY_SUCCESS,
	DELETE_BLOG_POST_REPLY_FAILURE,
	UPVOTE_BLOG_POST_REPLY,
	UPVOTE_BLOG_POST_REPLY_SUCCESS,
	UPVOTE_BLOG_POST_REPLY_FAILURE
} from '../actions/blog'

const initialState = []


function getblog(state = {
	isFetching: false,
	didInvalidate: false,
	items: []
}, action) {
	let actionObject = {
		REQUEST_BLOG_POSTS: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		REQUEST_BLOG_POSTS_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			items: action.blog,
			lastUpdated: action.receivedAt
		}),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']
}

function handleBlogActions(state, action) {
	return Object.assign({}, state, {
		posts: getblog(state.blog, action)
	})
}
function blog(state, action) {
	if (typeof state === "undefined") {
		return initialState
	}
	let actionObject = {
		REQUEST_BLOG_POSTS: handleBlogActions(state, action),
		REQUEST_BLOG_POSTS_SUCCESS: handleBlogActions(state, action),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']	
}

export default blog