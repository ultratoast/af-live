import {
	//forum
	REQUEST_FORUM_POSTS,
	REQUEST_FORUM_POSTS_SUCCESS,
	REQUEST_FORUM_POSTS_FAILURE,
	REQUEST_ALL_FORUM_POSTS,
	REQUEST_ALL_FORUM_POSTS_SUCCESS,
	REQUEST_ALL_FORUM_POSTS_FAILURE,
	REQUEST_ONE_FORUM_POST,
	REQUEST_ONE_FORUM_POST_SUCCESS,
	REQUEST_ONE_FORUM_POST_FAILURE,
	ADD_FORUM_POST,
	ADD_FORUM_POST_SUCCESS,
	ADD_FORUM_POST_FAILURE,
	EDIT_FORUM_POST,
	EDIT_FORUM_POST_SUCCESS,
	EDIT_FORUM_POST_FAILURE,
	DELETE_FORUM_POST,
	DELETE_FORUM_POST_SUCCESS,
	DELETE_FORUM_POST_FAILURE,
	UPVOTE_FORUM_POST,
	UPVOTE_FORUM_POST_SUCCESS,
	UPVOTE_FORUM_POST_FAILURE,
	ADD_FORUM_POST_REPLY,
	ADD_FORUM_POST_REPLY_SUCCESS,
	ADD_FORUM_POST_REPLY_FAILURE,
	DELETE_FORUM_POST_REPLY,
	DELETE_FORUM_POST_REPLY_SUCCESS,
	DELETE_FORUM_POST_REPLY_FAILURE,
	UPVOTE_FORUM_POST_REPLY,
	UPVOTE_FORUM_POST_REPLY_SUCCESS,
	UPVOTE_FORUM_POST_REPLY_FAILURE
	//products
} from '../actions/forum'

const initialState = []

function getforum(state = {
	isFetching: false,
	didInvalidate: false,
	items: []
}, action) {
	let actionObject = {
		REQUEST_FORUM_POSTS: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		REQUEST_FORUM_POSTS_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			items: action.forum,
			lastUpdated: action.receivedAt
		}),
		REQUEST_FORUM_POSTS_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		REQUEST_ONE_FORUM_POST: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		REQUEST_ONE_FORUM_POST_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			items: action.post,
			lastUpdated: action.receivedAt
		}),
		REQUEST_ONE_FORUM_POST_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		ADD_FORUM_POST: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		ADD_FORUM_POST_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			items: action.posts,
			lastUpdated: action.receivedAt
		}),
		ADD_FORUM_POST_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		EDIT_FORUM_POST: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		EDIT_FORUM_POST_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			// items: state.forum.posts.items.push(action.post),
			lastUpdated: action.receivedAt
		}),
		EDIT_FORUM_POST_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		DELETE_FORUM_POST: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		DELETE_FORUM_POST_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			// items: state.forum.posts.items.push(action.post),
			lastUpdated: action.receivedAt
		}),
		DELETE_FORUM_POST_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		UPVOTE_FORUM_POST: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		UPVOTE_FORUM_POST_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			// items: state.forum.posts.items.push(action.post),
			lastUpdated: action.receivedAt
		}),
		UPVOTE_FORUM_POST_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		ADD_FORUM_POST_REPLY: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		ADD_FORUM_POST_REPLY_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			// items: state.forum.posts.items.push(action.post),
			lastUpdated: action.receivedAt
		}),
		ADD_FORUM_POST_REPLY_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		DELETE_FORUM_POST_REPLY: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		DELETE_FORUM_POST_REPLY_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			// items: state.forum.posts.items.push(action.post),
			lastUpdated: action.receivedAt
		}),
		DELETE_FORUM_POST_REPLY_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		UPVOTE_FORUM_POST_REPLY: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		UPVOTE_FORUM_POST_REPLY_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			// items: state.forum.posts.items.push(action.post),
			lastUpdated: action.receivedAt
		}),
		UPVOTE_FORUM_POST_REPLY_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']
}

function handleForumActions(state, action) {
	return Object.assign({}, state, {
		posts: getforum(state.forum, action)
	})
}
function forum(state, action) {
	if (typeof state === "undefined") {
		return initialState
	}
	let actionObject = {
		REQUEST_FORUM_POSTS: handleForumActions(state, action),
		REQUEST_FORUM_POSTS_SUCCESS: handleForumActions(state, action),
		REQUEST_FORUM_POSTS_FAILURE: handleForumActions(state,action),
		REQUEST_ONE_FORUM_POST: handleForumActions(state,action),
		REQUEST_ONE_FORUM_POST_SUCCESS: handleForumActions(state,action),
		ADD_FORUM_POST: handleForumActions(state,action),
		ADD_FORUM_POST_SUCCESS: handleForumActions(state,action),
		ADD_FORUM_POST_FAILURE: handleForumActions(state,action),
		EDIT_FORUM_POST: handleForumActions(state,action),
		EDIT_FORUM_POST_SUCCESS: handleForumActions(state,action),
		EDIT_FORUM_POST_FAILURE: handleForumActions(state,action),
		DELETE_FORUM_POST: handleForumActions(state,action),
		DELETE_FORUM_POST_SUCCESS: handleForumActions(state,action),
		DELETE_FORUM_POST_FAILURE: handleForumActions(state,action),
		UPVOTE_FORUM_POST: handleForumActions(state,action),
		UPVOTE_FORUM_POST_SUCCESS: handleForumActions(state,action),
		UPVOTE_FORUM_POST_FAILURE: handleForumActions(state,action),
		ADD_FORUM_POST_REPLY: handleForumActions(state,action),
		ADD_FORUM_POST_REPLY_SUCCESS: handleForumActions(state,action),
		ADD_FORUM_POST_REPLY_FAILURE: handleForumActions(state,action),
		DELETE_FORUM_POST_REPLY: handleForumActions(state,action),
		DELETE_FORUM_POST_REPLY_SUCCESS: handleForumActions(state,action),
		DELETE_FORUM_POST_REPLY_FAILURE: handleForumActions(state,action),
		UPVOTE_FORUM_POST_REPLY: handleForumActions(state,action),
		UPVOTE_FORUM_POST_REPLY_SUCCESS: handleForumActions(state,action),
		UPVOTE_FORUM_POST_REPLY_FAILURE: handleForumActions(state,action),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']	
}

export default forum