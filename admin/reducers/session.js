import {
	REQUEST_SESSION,
	REQUEST_SESSION_SUCCESS,
	REQUEST_SESSION_FAILURE,
	DELETE_SESSION,
	DELETE_SESSION_SUCCESS,
	DELETE_SESSION_FAILURE
} from '../actions/session'

const initialState = []



function getSession(state = {
	isFetching: false,
	didInvalidate: false,
	items: {}
}, action) {
	let actionObject = {
		REQUEST_SESSION: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		REQUEST_SESSION_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			items: action.session,
			lastUpdated: action.receivedAt
		}),
		REQUEST_SESSION_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		DELETE_SESSION: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		DELETE_SESSION_SUCCESS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			items: action.session,
			lastUpdated: action.receivedAt
		}),
		DELETE_SESSION_FAILURE: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: true,
			lastUpdated: action.receivedAt
		}),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']
}

function handleSessionActions(state, action) {
	return Object.assign({}, state, {
		session: getSession(state.session, action)
	})
}

function session(state, action) {
	if (typeof state === "undefined") {
		return initialState
	}
	let actionObject = {
		REQUEST_SESSION: handleSessionActions(state, action),
		REQUEST_SESSION_SUCCESS: handleSessionActions(state, action),
		REQUEST_SESSION_FAILURE: handleSessionActions(state, action),
		DELETE_SESSION: handleSessionActions(state, action),
		DELETE_SESSION_SUCCESS: handleSessionActions(state, action),
		DELETE_SESSION_FAILURE: handleSessionActions(state, action),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']
}

export default session 