require('es6-promise').polyfill()

const config = require('../config'),
	Express = require('express'),
	React = require('react'),
	ReactDOMServer = require('react-dom/server'),
	RouterContext = require('react-router/lib/RouterContext'),
	Router = Express.Router(),
	router = require('react-router').Router,
	Provider = require('react-redux/lib/components/Provider'),
	createStore = require('redux/lib/createStore').default,
	applyMiddleware = require('redux/lib/applyMiddleware').default,
	thunk = require('redux-thunk').default,
	match = require('react-router/lib/match'),
	routes = require('./routes'),
	blogActions = require('./actions/blog'),
	forumActions = require('./actions/forum'),
	productsActions = require('./actions/products'),
	mainReducer = require('./main-reducer').default,
	jwt = require('jsonwebtoken'),
	secret = config.jwt.secret

if (config.env == "dev") {
	//dev only
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
}

function fetchData(store,next) {
	Promise.all([
		store.dispatch(blogActions.getBlogPostsIfNeeded()),
		store.dispatch(forumActions.getForumPostsIfNeeded()),
		// store.dispatch(actions.getUsersIfNeeded()),
		store.dispatch(productsActions.getProductsIfNeeded())
		])
	.then((res) => {
		console.log('fetch response',res)
		next()
	})
	.catch(err => {
		console.log('fetch error', err)
	})
}

Router.route('*')
	.get(function(req,res,next){
			// console.log('routeurl '+req.url)
	 	match({ routes:routes, location: req.url }, (error, redirectLocation, renderProps) => {
		    if (error) {
		    	//return the error view
		      	res.render('misc/error',{message:error.message,error:error})
		    } else if (redirectLocation) {
		    	//follow redirect
		      	res.redirect(302, redirectLocation.pathname + redirectLocation.search)
		    } else if (renderProps) {
		    	//render redux + react router
      			let store = applyMiddleware(thunk)(createStore)(mainReducer)
				fetchData(store,() => {
					console.log('inside async')
					let initialState = store.getState(),
						provider = React.createFactory(Provider.default),
	      				routerContext = React.createFactory(RouterContext)
	      			renderProps.params = Object.assign(renderProps.params,initialState)
					let component = provider({store: store},
						routerContext(renderProps)
					)
					res.send(ReactDOMServer.renderToStaticMarkup(component))
			      	// res.send(ReactDOMServer.renderToString(component))
				})
		    } else {
		      	res.send('Not found')
		    }
		    // next()
	  	})
	})

module.exports = Router