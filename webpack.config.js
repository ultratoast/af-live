module.exports = {
  context: __dirname,
    entry: {
      admin: "./admin/index.js",
      // server: "./components/app.js",
      client: "./components/client.js"
    },
    output: {
        path: __dirname + "/dist/js",
        filename: "[name].bundle.js"
    },
 module: {
    noParse: /node_modules\/quill\/dist/,
   loaders: [
     {
      test: /\.jsx?$/,
      exclude: /(node_modules|bower_components|api)/,
      loader: 'babel', // 'babel-loader' is also a legal name to reference
      query: {
        presets: ['react', 'es2015']
      }
    },
     {
      test: /\.js?$/,
      exclude: /(node_modules|bower_components|api)/,
      loader: 'babel', // 'babel-loader' is also a legal name to reference
      query: {
        presets: ['react', 'es2015']
      }
    },
     {
      test: /\.json$/,
      loaders: ['json']
     }
   ]
 },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json']
  }
};