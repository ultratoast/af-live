$(document).ready(function(){
	$('.search').on('click', function() {
		var filter = $('.filter').val(),
			$table = $('table'),
			cells = $table.find('td')
		if (filter.length > 1) {
			for (var i = 0;i<cells.length;i++) {
				var $cell = $(cells[i])
				var cellValue = $cell.attr('data-value')
				if (cellValue.indexOf(filter) == -1 && !$cell.parent('tr').hasClass('safe')) {
					$cell.parent('tr').hide()
				} else {
					$cell.parent('tr').show().addClass('safe')
				}
			}
		} else {
			$('tr').show().removeClass('safe')
		}
	})
})