const Mongoose = require('mongoose'),
	ObjectId = Mongoose.Schema.Types.ObjectId,
	ProductSchema = require('./product')

const KeySchema = new Mongoose.Schema({
	product: {type: ObjectId, ref: 'Product'},
	license: {type:String, unique: true},
	soldOn: {type: Date, default: Date.now},
	activations: [{type:ObjectId, ref:'Activation'}]
})

const ActivationSchema = new Mongoose.Schema({
	activationDate: Date,
	hash: {type:ObjectId, ref: 'HashedID'},
	user: {type: ObjectId, ref: 'User'},
	license: {type:ObjectId, ref: 'Key'},
	isActive: Boolean,
	ua: String,
	ip: String,
	product: {type: ObjectId, ref: 'Product'}
})

const HashedIDSchema = new Mongoose.Schema({
	hash: String,
	storedOn: { type: Date, Default: Date.now},
	license: {type: ObjectId, ref: 'Key'},
	user: {type: ObjectId, ref: 'User'}
})


Mongoose.model('Key',KeySchema)
Mongoose.model('HashedID',HashedIDSchema)
Mongoose.model('Activation',ActivationSchema)