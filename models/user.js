const Mongoose = require('mongoose'),
	ObjectId = Mongoose.Schema.Types.ObjectId

const UserSchema = new Mongoose.Schema({
	email: String,
	accessLevel: String,
	firstName: String,
	lastName: String,
	nickName: String,
	company: String,
	experience: String,
	website: String,
	ip: String,
	sendNewsletter: Boolean,
	registeredOn: {type: Date, Default: Date.now},
	registeredFrom: String,
	purchases: [{type: ObjectId, ref: 'Purchase'}],
	cart: [{type: ObjectId, ref: 'Product'}]
})

const PasswordSchema = new Mongoose.Schema({
	user: {type: ObjectId, ref: 'User'},
	pstring: String
})


Mongoose.model('User', UserSchema)
Mongoose.model('Password', PasswordSchema)