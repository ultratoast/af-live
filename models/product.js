const Mongoose = require('mongoose'),
	ObjectId = Mongoose.Schema.Types.ObjectId

const ProductSchema = new Mongoose.Schema({
	name: String,
	categories: [{type:ObjectId, ref:'ProductCategory'}],
	description: String,
	price: Number,
	sku: String,
	quantity: {type:Number, min: 0},
	imageURL: String,
	downloadURL: String,
	productKey: {type:ObjectId,ref: 'Key'},
	postDate: {type: Date,default: Date.now }
})

const ProductCategorySchema = new Mongoose.Schema({
	name: String
})

const PurchaseSchema = new Mongoose.Schema({
	user: {type: ObjectId, ref: 'User'},
	product: {type: ObjectId, ref: 'Product'},
	purchaseDate: {type: Date, default: Date.now},
	purchasePrice: Number
})

Mongoose.model('Purchase', PurchaseSchema)
Mongoose.model('Product', ProductSchema)
Mongoose.model('ProductCategory',ProductCategorySchema)