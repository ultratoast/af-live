const Mongoose = require('mongoose'),
	ObjectId = Mongoose.Schema.Types.ObjectId

const SessionSchema = new Mongoose.Schema({
	user: { type: ObjectId, ref: 'User'},
	startedOn: Date,
	expiresOn: Date,
	token: String
})


Mongoose.model('Session',SessionSchema)