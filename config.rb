preferred_syntax = :sass
css_dir = 'dist/css'
sass_dir = 'dist/scss'
images_dir = 'dist/img'
javascripts_dir = 'dist/js'
relative_assets = true
output_style = :compressed