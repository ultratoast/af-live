"use strict"

const gulp = require('gulp'),
	webpack = require('webpack'),
	nodemon = require('nodemon'),
	config = require('./webpack.config.js')

function onBuild(done) {
  return function(err, stats) {
    if(err) {
      console.log('Error', err);
    }
    else {
      console.log(stats.toString());
    }

    if(done) {
      done();
    }
  }
}

gulp.task('build', function(done) {
  webpack(config).run(onBuild(done));
});

gulp.task('watch', function() {
  webpack(config).watch(100, onBuild());
});

gulp.task('run', ['watch'], function() {
  nodemon({
    execMap: {
      js: 'node'
    },
    script: './components/app',
    ignore: ['./dist/'],
    watch: ['./*'],
    ext: 'noop'
  }).on('restart', function() {
    console.log('Restarted!');
  });
});