function renderJSON(res,status,message,data = {}) {
	let msgKey = '',
		msgValue = '',
		json = {}
	Object.keys(message).forEach(function(key){
	    msgValue = message[key]
	    msgKey = key
	});
	json[msgKey] = msgValue
	res.status(status || 200).json(json)
}

module.exports = renderJSON