"use strict";

const crypto = require('crypto')
const SECRET_PHRASE = "kendrickLamar"
const SECRET_KEY = "12a54f72e3cb7d8f"
const iv = crypto.randomBytes(8).toString('hex')

const cipher = (password) => {
	var cipher = crypto.createCipher("aes192", SECRET_PHRASE)
	return cipher.update(password,"binary","hex") + cipher.final("hex")
}
const decipher = (password) => {
	var decipher = crypto.createDecipher("aes192", SECRET_PHRASE)
	return decipher.update(password, "hex", "binary") + decipher.final("binary")
}

const ivcipher = (phrase) => {
	var ivcipher = crypto.createCipheriv("aes-128-cbc", SECRET_KEY, iv)
	var final = ivcipher.update(phrase,'binary','base64') + ivcipher.final('base64')
	return {'phrase': final, 'iv':iv}
}

const ivdecipher = (phrase,iv) => {
	var ivdecipher = crypto.createDecipheriv("aes-128-cbc", SECRET_KEY, iv)
	var final = ivdecipher.update(phrase,'base64','binary') + ivdecipher.final('binary')
	return final
}


module.exports = {cipher, decipher, ivcipher, ivdecipher}