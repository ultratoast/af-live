const Mongoose = require('mongoose'),
	db = require('../../../models/db'),
	product = require('../../../models/product'),
	user = require('../../../models/user'),
	post = require('../../../models/post'),
	key = require('../../../models/key'),
	session = require('../../../models/session')

const dbHelper = {
	leanFind: (model,options,next) => {
		Mongoose.model(model).find(options).lean().exec((err,data) => {
			if (err) {
				next(err)
			} else {
				next(null,data)
			}

		})
	},
	findOne: (model,id,next) => {
		id = id.toString()
		if (id.match(/^[0-9a-fA-F]{24}$/)) {
		  	// Yes, it's a valid ObjectId, proceed with `findById` call.
			Mongoose.model(model).findById(id).lean().exec((err,data) => {
				if (err) {
					next(err)
				} else {
					next(null,data)
				}
			})
		} else {
			next('Error, invalid ID')
		}
	},
	findOneByKey: (model,options,next) => {
		Mongoose.model(model).findOne(options).lean().exec((err,data) => {
			if (err) {
				next(err)
			} else {
				next(null,data)
			}
		})
	},
	addOne: (model,options,next) => {
		Mongoose.model(model).create(options, (err,data) => {
			if (err) {
				next(err)
			} else {
				next(null,data)
			}
		})
	},
	deleteOne: (model,id,next) => {
		id = id.toString()
		if (id.match(/^[0-9a-fA-F]{24}$/)) {
 			 // Yes, it's a valid ObjectId, proceed with `findById` call.
				Mongoose.model(model).remove(id, (err,data) => {
				if (err) {
					next(err)
				} else {
					next(null,data)
				}
			})
		} else {
			next('Error, Invalid ID')
		}
	},
	updateOne: (model,id,options,next) => {
		id = id.toString()
		if (id.match(/^[0-9a-fA-F]{24}$/)) {
 		 // Yes, it's a valid ObjectId, proceed with `findById` call.
		Mongoose.model(model).findById(id, (err,item) => {
			if (err) {
				next(err)
			} else {
				item.update(options, (err, data) => {
					if (err) {
						next(err)
					} else {
						next(null, data)
					}
				})
			}
		})
		} else {
			next('Error, Invalid Id')
		}
	}
}

module.exports = dbHelper