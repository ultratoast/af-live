const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	renderJSON = require('../helpers/renderJSON'),
	guard = require('express-jwt-permissions')()

Router.route('/:type/all')
	.get((req,res) => {
		let type = req.params.type,
			findStatement = {
			type: type
		}
		dbHelper.leanFind('Post',findStatement,(err,posts) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.posts = posts
			}
			renderJSON(res,code,{'GetAllPosts':resData})
		})
	})

Router.route('/:type/latest')
	.get((req,res) => {
		let type= req.params.type,
			now = new Date(Date.now()),
			pastMonth = new Date(now.getTime() - 30 * 24 * 60 * 60 * 1000),
			findStatement = {
				type,
				postDate: {$lte:now,$gte:pastMonth}
			}
		dbHelper.leanFind('Post',findStatement,(err,posts) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.posts = posts
			}
			renderJSON(res,code,{'GetLatestPosts':resData})
		})
	})


Router.route('/:type/add')
	.post(guard.check("flog"), (req,res) => {
		let type= req.params.type,
			postDate = new Date(),
			data = req.body.post,
			categories = data.categories.length > 0 ? data.categories.split(',') : []
		let createStatement = {
			title: data.title,
			author: data.author,
			postDate: postDate,
			content: data.content,
			isPublic: data.isPublic,
			catgeories: categories,
			type: data.type,
			upvotes: 0
		}
		dbHelper.addOne('Post',createStatement, (err,post) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.post = post
			}
			renderJSON(res,code,{'AddPost':resData})
		})
	})

Router.route('/:type/:id')
	.get(guard.check("session"),(req,res) => {
		let type= req.params.type
		dbHelper.findOne('Post',req.params.id, (err,post) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.post = post
			}
			renderJSON(res,code,{'GetPost':resData})
		})
	})

Router.route('/:type/:id/upvote')
	//post upvotes
	.post(guard.check("session"), (req,res) => {
		let type= req.params.type,
			upvotes = req.body.upvotes,
			user = req.body.user,
			updateStatement = {
				upvotes: upvotes
			}
		dbHelper.findOne('User',user, (err,user) => {
			let resData = {
				message: 'Success'
			},
			code = 200
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				let upvoteArr = user.upvotes,
					alreadyVoted = upvoteArr.find(req.params.id)
				if (alreadyVoted == 'undefined') {
					dbHelper.updateOne('Post',req.params.id,updateStatement, (err,post) => {
						if (err) {
							code = 500
							resData.message = 'Error'
							resData.error = err
						} else {
							resData.post = post
						}
						renderJSON(res,code,{'PostUpvote':resData})
					})
				} else {
					code = 500
					resData.message = 'Error'
					resData.error = 'Already Voted'
					renderJSON(res,code,{'PostUpvote':resData})
				}
			}
		})
	})

Router.route('/:type/:id/reply')
	//add reply
	.post(guard.check("session"),(req,res) => {
		let type= req.params.type,
			now = new Date(),
			createStatement = {
				author: req.body.author,
				content: req.body.content,
				postDate: now,
				upvotes: 0,
				thread: req.params.id
			}
		dbHelper.addOne('Reply',createStatement,(err,reply) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.reply = reply
			}
			renderJSON(res,code,{'AddReply':resData})
		})
	})

Router.route('/:type/:id/reply/:replyid')
	//handle reply upvotes
	.post(guard.check("session"),(req,res) => {
		let type= req.params.type,
			upvotes = req.body.upvotes,
			updateStatement = {
				upvotes: upvotes
			} 
		dbHelper.updateOne('Reply',req.params.replyid, updateStatement, (err,reply) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.reply = reply
			}
			renderJSON(res,code,{'ReplyUpvote':resData})
		})
	})
	//handle reply deletion
	.delete(guard.check("session"),(req,res) => {
		let type= req.params.type
		dbHelper.deleteOne('Reply',req.params.id, (err,reply) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.reply = reply
			}
			renderJSON(res,code,{'DeleteReply':resData})
		})
	})


Router.route('/:type/:id/edit')
	.post(guard.check("flog"), (req,res) => {
		let categories = []
		req.body.categories.map((category,i) => {
			categories.push(category)
		})
		let updateStatement = {
			title: req.bodytitle,
			content: req.bodycontent,
			isPublic: req.bodyisPublic,
			categories: catgeories
		}
		dbHelper.updateOne('Post',req.params.id,updateStatement, (err,post) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.post = post
			}
			renderJSON(res,code,{'UpdatePost':resData})
		})
	})

Router.route('/:type/:id/delete')
	.delete(guard.check("flog"), (req,res) => {
		dbHelper.deleteOne('Post',req.params.id, (err,post) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.post = post
			}
			renderJSON(res,code,{'DeletePost':resData})
		})
	})

module.exports = Router