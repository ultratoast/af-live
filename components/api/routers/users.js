const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	renderJSON = require('../helpers/renderJSON'),
	guid = require('../helpers/guid'),
	crypty = require('../helpers/crypty'),
	jwt = require('jsonwebtoken'),
	config = require('../../../config'),
	guard = require('express-jwt-permissions')()

Router.route('/login') //verify account + password + start session
	.post((req,res) => {
		let resData = {
			message: 'Success'
		},
		code = 200,
		credentials = req.body.credentials
		//first find the user
		dbHelper.findOneByKey('User',{email:credentials.email}, (err,user) => {
			if (err || user == null) {
				code = 500
				resData.message = 'Error, User Not Found'
				resData.error = err
				renderJSON(res,code,{'Login':resData})
			} else {
				let formpassword = crypty.cipher(credentials.password),
					accessLevel = user.accessLevel
				dbHelper.findOneByKey('Password',{user:user._id}, (err,password) => {
					if (err || password == null) {
						code = 500
						resData.message = 'Error Retrieving Password'
						resData.error = err
						renderJSON(res,code,{'Login':resData})
					} else {
						if (password && formpassword && formpassword === password.pstring) {
							//if password matches, generate a JWT
							let accessObj = {
								"admin": ["users","cms","flog","ecommerce","session","license"],
								"user" : ["flog","ecommerce","session","license"],
								"anon": [],
								"default": []
							},
								permissions = {
									"permissions": accessObj[accessLevel]
								},
								token = jwt.sign(permissions, config.jwt.secret, { expiresIn: 3600*30 })
							let startedOn = new Date(Date.now()),
								expiresOn = new Date(startedOn.getTime() + 30 * 24 * 60 * 60 * 1000), //one month sessions
								sessionObj = {
								id: '',
								user: user,
								startedOn: startedOn,
								expiresOn: expiresOn,
								token: token
							}
							//Now check for existing session and update if necessary
							dbHelper.findOneByKey('Session',{user:user._id}, (err, session) => {
								if (err) {
									code = 500
									resData.message = 'Error Retrieving Session'
									resData.error = err
									renderJSON(res,code,{'Login':resData})								
								} else if (!session || session == null) {
									//no session found, time to make one
									dbHelper.addOne('Session',sessionObj, (err, session) => {
										sessionObj.id = session._id
										resData.session = sessionObj
										renderJSON(res,code,{'Login':resData})
									})
								} else {
									dbHelper.updateOne('Session',session._id, sessionObj, (err,success) => {
										if (session.expiresOn > startedOn && session.token != null) {
											sessionObj.token = session.token
										}
										sessionObj.id = session._id
										resData.session = sessionObj
										renderJSON(res,code,{'Login':resData})
									})
								}
							})
						} else {
							code = 401
							resData.message = 'Error, Missing Password or Password Mismatch'
							resData.error = err
							renderJSON(res,code,{'Login':resData})
						}
					}
				})
			}
		})
	})
Router.route('/logout') //kill session
	.post(guard.check("session"), (req,res) => {
		let updateStatement = {
				expiresOn: Date.now(),
				token: null
			}
		dbHelper.updateOne('Session',req.body.sessionid,updateStatement, (err,session) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err || session == null) {
				code = 500
				resData.message = 'Error Retrieving Session'
				resData.error = err
			} else {
				resData.session = {}
			}
			renderJSON(res,code,{'Logout':resData})
		})
	})

Router.route('/create')
	.post((req,res) => {
		let today = new Date(),
			password,
			addStatement = {
				email: req.body.email,
				sendNewsletter: req.body.sendNewsletter || true
			},
			findStatement = {
				email: req.body.email,
			}
		//configure the user object depending on what kind of form was filled out on the front end
		if (req.body.registeredFrom == 'beta') {
			addStatement.experience = req.body.experience
			addStatement.firstname = req.body.firstname
			addStatement.lastname = req.body.lastname
			addStatement.accessLevel = "anon"
		} else if (req.body.registeredFrom == 'newsletter') {
			addStatement.company = req.body.company
			addStatement.accessLevel = "anon"
		} else if (req.body.registeredFrom == 'normal') {
			addStatement.firstname = req.body.firstname
			addStatement.lastname = req.body.lastname
			addStatement.experience = req.body.experience
			addStatement.accessLevel = "user"
			//encode the password for storage
			password = req.body.password ? crypty.cipher(req.body.password) : null
		}
		//get the user with the email provided
		dbHelper.findOneByKey('User',findStatement,(err,user) => {
			let resData = {
				message: 'Success'
			},
			code = 200 
			if (err) {
				code = 500
				resData.message = 'Database Error'
				resData.error = err
				renderJSON(res,code,{'AddUser':resData})
			//if the user does not exist, create one			
			} else if (!user || user.length == 0) {
				addStatement.registeredOn =  today
				addStatement.registeredFrom = req.body.registeredFrom
				dbHelper.addOne('User',addStatement,(err,user) => {
					if (err) {
						code = 500
						resData.message = 'Database Error'
						resData.error = err
						renderJSON(res,code,{'AddUser':resData})
					} else {
						resData.user = user
						//check the request for a password and confirm the type is correct as well
						if (req.body.registeredFrom == 'normal' && password && password != null && password != 'undefined') {
							let passwordAddStatement = {
								pstring: password,
								user: user._id
							}
							//make sure the passwords match
							if (req.body.password === req.body.password2) {
								dbHelper.findOneByKey('Password', {user: user._id}, (err,password) => {
									if (err) {
										code = 500
										resData.message = 'Database Error'
										resData.error = err
										renderJSON(res,code,{'AddUser':resData})
									} else if (!password || password.length == 0) {
										//save the password
										dbHelper.addOne('Password', passwordAddStatement, (err,password)=> {
											if (err) {
												code = 500
												resData.message = 'Database Error'
												resData.error = err
											}
											renderJSON(res,code,{'AddUser':resData})
										})
									} else {
										code = 500
										resData.message = 'Error, User Exists'
										resData.error = err
										renderJSON(res,code,{'UpdateUser':resData})
									}
								})
							} else {
								code = 500
								resData.message = 'Error, Password Mismatch'
								resData.error = err
								renderJSON(res,code,{'AddUser':resData})	
							}
						} else {
							renderJSON(res,code,{'AddUser':resData})
						}
					}
				})
			} else {
				resData.user = user
				//if the user exists, we need to verify this isn't already a fully registered account
				dbHelper.findOneByKey('Password', {user: resData.user._id}, (err,pwdobj) => {
					if (err) {
						code = 500
						resData.message = 'Database Error'
						resData.error = err
						renderJSON(res,code,{'UpdateUser':resData})
					} else if (!pwdobj || pwdobj.length == 0) {
						//if the password isn't found we check the form type and for a password
						if (req.body.registeredFrom == 'normal' && password && password != null && password != 'undefined') {
							let passwordAddStatement = {
								pstring: password,
								user: resData.user._id
							}
							//make sure the passwords match
							if (req.body.password === req.body.password2) {
								//save the password
								dbHelper.addOne('Password', passwordAddStatement, (err,pwdobj)=> {
									if (err) {
										code = 500
										resData.message = 'Database Error'
										resData.error = err
									}
									//update the user with new info
									dbHelper.updateOne('User', user._id, addStatement, (err, user) => {
										if (err) {
											code = 500
											resData.message = 'Error'
											resData.error = err
										}
										renderJSON(res,code,{'UpdateUser':resData})
									})
								})
							} else {
								code = 500
								resData.message = 'Error, Password Mismatch'
								resData.error = err
								renderJSON(res,code,{'UpdateUser':resData})	
							}
						} else {
							dbHelper.updateOne('User', user._id, addStatement, (err, user) => {
								if (err) {
									code = 500
									resData.message = 'Error'
									resData.error = err
								}
								renderJSON(res,code,{'UpdateUser':resData})
							})
						}
					} else {
						code = 500
						resData.message = 'Error, User Exists'
						resData.error = err
						renderJSON(res,code,{'UpdateUser':resData})
					}
				})
			}
		})
	})

Router.route('/all') //get all users
	.get(guard.check("users"), (req,res)  => {
		console.log('users request',req)
		dbHelper.leanFind('User',{},(err,users) => {
			let resData = {
				message: 'Success'
			},
			code = 200
			if (err) {
				code = 500
				resData.message = "Error"
				resData.error = err
			} else {
				resData.users = users
			}
			renderJSON(res,code,{'GetAllUsers':resData})
		})
	})

Router.route('/:uid') //get + post + delete
	.get(guard.check("session"), (req,res) => {
		dbHelper.findOne('User',req.params.uid, (err,user) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.user = user
			}
			renderJSON(res,code,{'GetUser':resData})
		})
	})
	.post(guard.check("session"),(req,res) => {
		let updateStatement = {
				email: req.body.email,
				firstname: req.body.firstname,
				lastname: req.body.lastname,
				nickname: req.body.nickname,
				experience: req.body.experience,
				company: req.body.company,
				website: req.body.company,
				sendNewsletter: req.body.sendNewsletter		
			}
		dbHelper.updateOne('User',req.params.uid,updateStatement, (err,user) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.user = user
			}
			renderJSON(res,code,{'AddUser':resData})
		})
	})
	.delete(guard.check("session"),(req,res) => {
		dbHelper.deleteOne('User',req.params.uid, (err,user) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.user = user
			}
			renderJSON(res,code,{'DeleteUser':resData})
		})
	})
module.exports = Router