const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	renderJSON = require('../helpers/renderJSON'),
	guard = require('express-jwt-permissions')()

Router.route('/buy/:pid')
	.post(guard.check("ecommerce"),(req,res) => {
		let resData = {
			message: 'Success'
		},
			code = 200
		//get the product type just purchased
		dbHelper.findOne('Product',req.params.pid,(err,product) => {
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
				renderJSON(res,code,{'Purchase':resData})
			} else {
				let productObj = product,
					userObj = Session.user,
					updateStatement = {
						purchases:  userObj.purchases.push(productObj) 
					}
				//add the product to the user's purchases
				dbHelper.updateOne('User',user.id,updateStatement, (err,user) => {
					if (err) {
						code = 500
						resData.message = 'Error'
						resData.error = err
						renderJSON(res,code,{'Purchase':resData})
					} else {
						let userUpdate = user,
							updateStatement = {
								quantity: productObj.quantity - 1
							}
						//Update the product in the database by removing one
						dbHelper.updateOne('Product',req.params.pid,updateStatement, (err, product) => {
							if (err) {
								code = 500
								resData.message = 'Error'
								resData.error = err
								renderJSON(res,code,{'Purchase':resData})
							} else {
								let today = new Date(),
									addStatement = {
										user: userUpdate.id,
										product: product.id,
										purchaseDate: today,
										purchasePrice: produce.price
									}
								productObj = product
								//register a new purchase
								dbHelper.addOne('Purchase',addStatement, (err,purchase) => {
									if (err) {
										code = 500
										resData.message = 'Error'
										resData.error = err
										renderJSON(res,code,{'Purchase':resData})
									} else {
										//update the user's cart to reflect the purchase
										dbHelper.updateOne('Cart', Session.cart.id, updateStatement, (err, cart) => {
											if (err) {
												code = 500
												resData.message = 'Error'
												resData.error = err
											} else {
												resData.cart = cart
												resData.user = userUpdate
											}
											renderJSON(res,code,{'Purchase':resData})
										})
									}
								})
							}
						})
					}
				})
			}
		})
	})
	
Router.route('/add')
	.post(guard.check("cms"),(req,res) => {
		let addStatement = {
			name:req.body.name,
			categories: [],
			description: req.body.description,
			price: req.body.price,
			quantity: req.body.quantity,
			imageUrl: req.body.imageUrl,
			downloadUrl: req.body, downloadUrl,
			productKey: req.body.productKey,
			sku: req.body.sku
		}
		req.body.categories.map((category,i) => {
			addStatement.categories.push(category)
		})
		dbHelper.addOne('Product',addStatement, (err,product) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.product = product
			}
			renderJSON(res,code,{'AddProduct':resData})
		})
	})
Router.route('/:pid')
	.get(guard.check("cms"), (req,res) => {
		dbHelper.findOne('Product',req.params.pid, (err,product) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.product = product
			}
			renderJSON(res,code,{'GetProduct':resData})
		})
	})
Router.route('/:pid/edit')
	.post(guard.check("cms"), (req,res) => {
		let updateStatement = {
			name:req.body.name,
			categories: [],
			description: req.body.description,
			price: req.body.price,
			quantity: req.body.quantity,
			imageUrl: req.body.imageUrl,
			downloadUrl: req.body, downloadUrl,
			sku: req.body.sku
		}
		req.body.categories.map((category,i) => {
			addStatement.categories.push(category)
		})
		dbHelper.updateOne('Product',req.params.pid,updateStatement, (err,product) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.product = product
			}
			renderJSON(res,code,{'UpdateProduct':resData})
		})
	})
Router.route('/:pid/delete')
	.delete(guard.check("cms"), (req,res) => {
		dbHelper.deleteOne('Product',req.params.pid, (err, product) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.product = product
			}
			renderJSON(res,code,{'DeleteProduct':resData})
		})
	})
	
module.exports = Router