const Express = require('express'),
	Router = Express.Router(),
	flog = require('./flog'),
	ecomm = require('./ecomm'),
	license = require('./license'),
	users = require('./users'),
	demo = require('./demo'),
	bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser')

Router.use(bodyParser.json())

Router.use(bodyParser.urlencoded({ extended: false }))

Router.use(cookieParser())

Router.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

Router.use('/products',ecomm)

Router.use('/keys',license)

Router.use('/users',users)

Router.use('/demo',demo)

Router.get('/', (req,res) => {
	res.send('Works!')
})

Router.all('*', flog) //i think its kind of fun to dump hackers into random blog/forum queries, don't you?

module.exports = Router