const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	product = require('./product'),
	paypal = require('./paypal'),
	renderJSON = require('../helpers/renderJSON')

Router.use('/paypal',paypal) //possible endpoint for paypal transactions

Router.use('/product',product)

Router.route('/category/:category?') //list all products, optionally from a specific category
	.get((req,res) => {
		let findStatement = {
			category: req.params.category || '*'
		}
		dbHelper.leanFind('Product',findStatement, (err,products) => {
			let resData = {
				message: 'Success'
			},
			code = 200 
			if (err) {
				code = 500
				resData.message = "Error"
				resData.error = err
			} else {
				resData.products = products
			}
			renderJSON(res,code,{'Category':resData})
		})
	})

module.exports = Router