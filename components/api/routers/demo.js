const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	renderJSON = require('../helpers/renderJSON'),
	guid = require('../helpers/guid'),
	crypty = require('../helpers/crypty')

Router.route('/verify')
	.post((req,res) => {
		console.log('Demo Verify Request ', req.body,'ip ',req.headers['x-forwarded-for'], 'UA ', req.headers['user-agent'])
		let cipherobj = crypty.ivcipher(req.body.instance)
		let data={
			message: 'Success',
			ip: req.headers['x-forwarded-for'],
			ua: req.headers['user-agent'],
			phrase: cipherobj.iv + '+' + cipherobj.phrase
		}
		renderJSON(res,200,{'Verify':data})
	})

Router.route('/activate')
	.post((req,res) => {
		console.log('Demo Activate Request ', req.body,'ip ',req.headers['x-forwarded-for'], 'UA ', req.headers['user-agent'])
		let cipherobj = crypty.ivcipher(req.body.instance),
			safeHash = crypty.ivcipher(guid()),
			safeUser = crypty.ivcipher(guid()),
			safeKey = crypty.ivcipher(guid()),
			safeProduct = crypty.ivcipher(guid())
		let data = {
			message: 'Success',
			activation: {
				activationDate: Date.now(),
				hash: safeHash.iv + '+' + safeHash.phrase,
				user: safeUser.iv + '+' + safeUser.phrase,
				license: safeKey.iv + '+' +safeKey.phrase,
				isActive: true,
				device: req.headers['user-agent'],
				ip: req.headers['x-forwarded-for'],
				product: safeProduct.iv + '+' + safeProduct.phrase,
				phrase: cipherobj.iv + '+' + cipherobj.phrase
			}	
		}
		renderJSON(res,200,{'Activate':data})
	})

Router.route('/decrypt')
	.post((req,res) => {
		console.log('Demo Decrypt Request ', req.body,'ip ',req.headers['x-forwarded-for'], 'UA ', req.headers['user-agent'])
		let data = {
			decipher: crypty.ivdecipher(req.body.phrase,req.body.iv),
			message: 'Success'
		}
		renderJSON(res,200,{'Decrypt':data})
	})

Router.route('/deactivate')
	.post((req,res) => {
		console.log('Demo Deactivate Request ', req.body,'ip ',req.headers['x-forwarded-for'], 'UA ', req.headers['user-agent'])
		let data = {
			message: 'Success'
		}
		renderJSON(res,200,{'Deactivate':data})
	})
	
module.exports = Router