const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	renderJSON = require('../helpers/renderJSON'),
	guid = require('../helpers/guid'),
	crypty = require('../helpers/crypty'),
	guard = require('express-jwt-permissions')()

Router.route('/verify') //compare encrypted hardware ID from native app with key stored in db
	.post(guard.check("license"),(req,res) => {
		let activationid = req.body.activationid,
			hash = crypty.ivdecipher(req.body.hash),
			ua = req.headers['user-agent'],
			ip = req.headers['x-forwarded-for'],
			resData = {
				ip: ip,
				ua: ua,
				message: 'Success'
			},
			code = 200
		dbHelper.findOne('Activation',activationid, (err, activation) =>{
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
				renderJSON(res,code,{'Verify':resData})
			} else {
				dbHelper.findOne('HashedID',activation.hash, (err,hashObj) => {
					if (err || hash !== hashObj.id) {
						code = 500
						resData.message = 'Error'
						resData.error = err || 'ID mismatch'
					} else {
						let safeData = crypty.ivcipher(hashObj.id)
						resData.phrase = safeData.iv + '+' + safeData.phrase
					}
					renderJSON(res,code,{'Verify':resData})
				})
			}
		})
	})
Router.route('/activate') //compare license key and save encrypted hardware ID from native app on first log in, create new activation entry in db
	.post(guard.check("license"), (req,res) => {
		let hash = req.body.hash,
			key = req.body.key,
			email = req.body.email,
			ip = req.headers['x-forwarded-for'],
			ua = req.headers['user-agent'],
			resData = {
				message: 'Success'
			},
			code = 200
		//first get the user
		dbHelper.findOneByKey('User',{email:email}, (err, user) => {
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
				renderJSON(res,code,{'Activate':resData})
			} else {
				let user = user
				//then verify that key is valid
				dbHelper.findOneByKey('Key',{license:key},(err,key) => {
					if (err) {
						code = 500
						resData.message = 'Error'
						resData.error = err
						renderJSON(res,code,{'Activate':resData})
					} else {
						let license = key,
							today = new Date(),
							addStatement = {
								user: user.id,
								license: license.id,
								hash: hash,
								storedOn: today
							}
						//then save the hash in the system
						dbHelper.addOne('HashedID',addStatement, (err, hashObj) => {
							if (err) {
								code = 500
								resData.message = 'Error'
								resData.error = err
								renderJSON(res,code,{'Verify':resData})
							} else {
								let addStatement = {
									user: user.id,
									license: license.id,
									hash: hashObj.id,
									isActive: true,
									activationDate: today,
									ua: ua,
									ip: ip,
									product: license.product
								}
								//finally create an activation and return it to the client for local storage
								dbHelper.addOne('Activate',addStatement, (err, activation) => {
									if (err) {
										code = 500
										resData.message = 'Error'
										resData.error = err
									} else {
										let safeHash = crypty.ivcipher(activation.hash),
											safeKey = crypty.ivcipher(activation.license),
											safeUser = crypty.ivcipher(activation.user),
											safeProduct = crypty.ivcipher(activation.product)
										activation.hash = safeHash.iv + '+' + safeHash.phrase
										activation.license = safeKey.iv + '+' + safeKey.phrase
										activation.user = safeUser.iv + '+' + safeUser.phrase
										activation.product = safeProduct.iv + '+' + safeProduct.phrase
										resData.activation = activation
									}
									renderJSON(res,code,{'Activate':resData})
								})
							}
						})
					}
				})
			}
		})
	})
Router.route('/deactivate') //disable license key, delete hashed ID
	.post(guard.check("license"), (req,res) => {
		let activationid = req.body.activationid,
			hashid = req.body.hashid,
			keyid = req.body.keyid,
			resData = {
				ip: ip,
				ua: ua,
				message: 'Success'
			},
			code = 200
		//remove activation 
		dbHelper.deleteOne('Activation',activationid, (err, activation) => {
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
				renderJSON(res,code,{'Deactivate':resData})
			} else {
				//find license key
				dbHelper.findOne('Key',keyid, (err, key) => {
					if (err) {
						code = 500
						resData.message = 'Error'
						resData.error = err
						renderJSON(res,code,{'Deactivate':resData})
					} else {
						//find activation id in sub document of key
						let activations = key.activations,
							index = activations.indexOf(activationid)
							if (index > -1) {
								activations.splice(index, 1)
							} 
						let	updateStatement = {
								activations: activations
							}
						//save the key less the old activation
						dbHelper.updateOne('Key',updateStatement, (err, key) => {
							if (err) {
								code = 500
								resData.message = 'Error'
								resData.error = err
								renderJSON(res,code,{'Deactivate':resData})
							} else {
								//finally, delete the hashed hardware id from our system
								dbHelper.deleteOne('HashedID', hashid, (err, hashObj) => {
									if (err) {
										code = 500
										resData.message = 'Error'
										resData.error = err
									}
									renderJSON(res,code,{'Deactivate':resData})
								})
							}
						})
					}
				})
			}
		})
	})
Router.route('/generate') //generate a unique license key and save it in the database with 0 activations
	.post(guard.check("license"), (req,res) => {
		let addStatement = {
			product: req.body.pid,
			license: guid(),
			soldOn: req.body.purchaseDate,
			activations: []
		}
		dbHelper.addOne('Key',addStatement,(err,key) => {
			let resData = {
					message: 'Success'
				},
				code = 200 
			if (err) {
				code = 500
				resData.message = 'Error'
				resData.error = err
			} else {
				resData.key = key
			}
			renderJSON(res,code,{'Generate':resData})
		})
	})
module.exports = Router