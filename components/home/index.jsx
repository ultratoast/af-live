const React = require('react'),
	Link = require('react-router/lib/Link'),
	Lightbox = require('../layouts/lightbox.jsx'),
	carousel = require('../layouts/carousel')

const HomeIndex = React.createClass({
	componentDidMount: function() {
		carousel()
	},
	render: function() {
		return (
			<section className="main" id="home">
				<ul className="carousel" id="home-main-carousel">
					<li className="slide active" style={{backgroundImage: 'url(img/header_home_3.png)'}}>
						<div className="slide-content">
							<p>Analog meets Virtual Reality</p>
							<Link to="/vas3d"><button>Learn</button></Link>
						</div>
					</li>
					<li className="slide" style={{backgroundImage: 'url(img/header_home_2.png)'}}>
						<div className="slide-content">
							<p>Training Tomorrow's<br /> Audio Professional.</p>
						</div>
					</li>
					<li className="slide" style={{backgroundImage: 'url(img/header_home_1.png)'}}>
						<div className="slide-content">
							<p className="push-down">
								A recording studio at your fingertips. 
							</p>
						</div>
					</li>
				</ul>
				<div data-target="home-main-carousel" className="carousel-nav nav-left"><i className="fa fa-chevron-left"></i></div>
				<div data-target="home-main-carousel" className="carousel-nav nav-right"><i className="fa fa-chevron-right"></i></div>
				<ul className="tripleCTA">
					<li className="cta"><Link to="/vas3d"><img name="Explore Virtual Analog Studio" src="img/icon_home_explore.png"/><span>Explore VAS</span></Link><p>Meet the World's first fully virtualized analog recording studio.</p><a href="/vas3d"><button>Learn More</button></a></li>
					<li className="cta"><a className="open-lightbox" data-lightbox="beta" href="#"><img name="Sign up to Beta Test Virtual Analog Studio" alt="Sign up to Beta Test Virtual Analog Studio" src="img/icon_home_beta.png"/><span>Beta</span></a><p>Are you interested in Virtual Reality? Help us test.</p><a className="open-lightbox" data-lightbox="beta" href="#"><button>Register</button></a></li>
					<li className="cta"><a className="open-lightbox" data-lightbox="newsletter" href="#"><img className="newsletter" src="img/icon_home_newsletter.png" alt="Sign up for the Audio Fusion Newsletter" name="Sign up for the Audio Fusion Newsletter"/><span>Newsletter</span></a><p>Get regular updates about the cool stuff we're working on.</p><a className="open-lightbox" href="#" data-lightbox="newsletter"><button>Sign Up</button></a></li>
				</ul>
				<Lightbox/>
			</section>
		)
	}
})

module.exports = HomeIndex