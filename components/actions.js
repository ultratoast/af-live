const fetch = require('isomorphic-fetch')

//action types

//blog
export const RECEIVE_BLOG_POSTS = 'RECEIVE_BLOG_POSTS'

export const REQUEST_BLOG_POSTS = 'REQUEST_BLOG_POSTS'

export const RECEIVE_ONE_BLOG_POST = 'RECEIVE_ONE_BLOG_POST'

export const REQUEST_ONE_BLOG_POST = 'REQUEST_ONE_BLOG_POST'

export const ADD_BLOG_POST = 'ADD_BLOG_POST'

export const EDIT_BLOG_POST = 'EDIT_BLOG_POST'

export const DELETE_BLOG_POST = 'DELETE_BLOG_POST'

export const UPVOTE_BLOG_POST = 'UPVOTE_BLOG_POST'

//forum

export const RECEIVE_FORUM_POSTS = 'RECEIVE_FORUM_POSTS'

export const REQUEST_FORUM_POSTS = 'REQUEST_FORUM_POSTS'

export const RECEIVE_ONE_FORUM_POST = 'RECEIVE_ONE_FORUM_POST'

export const REQUEST_ONE_FORUM_POST = 'REQUEST_ONE_FORUM_POST'

export const ADD_FORUM_POST = 'ADD_FORUM_POST'

export const EDIT_FORUM_POST = 'EDIT_FORUM_POST'

export const DELETE_FORUM_POST = 'DELETE_FORUM_POST'

export const UPVOTE_FORUM_POST = 'UPVOTE_FORUM_POST'

//user

export const RECEIVE_USERS = 'RECEIVE_USERS'

export const REQUEST_USERS = 'REQUEST_USERS'

export const RECEIVE_ONE_USER = 'RECEIVE_ONE_USER'

export const REQUEST_ONE_USER = 'REQUEST_ONE_USER'

export const ADD_USER = 'ADD_USER'

export const EDIT_USER = 'EDIT_USER'

export const DELETE_USER = 'DELETE_USER'

export const UPVOTE_USER = 'UPVOTE_USER'

//store

export const RECEIVE_STORE_PRODUCTS = 'RECEIVE_STORE_PRODUCTS'

export const REQUEST_STORE_PRODUCTS = 'REQUEST_STORE_PRODUCTS'

export const RECEIVE_ONE_STORE_PRODUCT = 'RECEIVE_ONE_STORE_PRODUCT'

export const REQUEST_ONE_STORE_PRODUCT = 'REQUEST_ONE_STORE_PRODUCT'

export const ADD_STORE_PRODUCT = 'ADD_STORE_PRODUCT'

export const EDIT_STORE_PRODUCT = 'EDIT_STORE_PRODUCT'

export const DELETE_STORE_PRODUCT = 'DELETE_STORE_PRODUCT'

export const UPVOTE_STORE_PRODUCT = 'UPVOTE_STORE_PRODUCT'

//other constants

export const BlogPostStatus = {
	DRAFT: 'DRAFT',
	PUBLISHED: 'PUBLISHED',
	DELETED: 'DELETED'
}

//action creators

//blog

export function addBlogPost(post) {
	return { type: ADD_BLOG_POST, post }
}

export function editBlogPost(post) {
	return { type: EDIT_BLOG_POST, post }
}

export function deleteBlogPost(post) {
	return { type: DELETE_BLOG_POST, post }
}

function requestBlogPosts(blog) {
	return { type: REQUEST_BLOG_POSTS, blog }
}

function receiveBlogPosts(blog, json) {
	return {
		type: RECEIVE_BLOG_POSTS,
		blog,
		posts: json.data.childen.map(child => child.data),
		receivedAt: Date.now()
	}
}

function getBlogPosts(blog) {
	return dispatch => {
		dispatch(requestBlogPosts(blog))
		return fetch('/api/blog/latest')
			.then(response => response.json())
			.then(json => dispatch(recievedBlogPosts(blog, json)))
	}
}

function shouldGetBlogPosts(blog) {
	if (!blog || blog.length == 0) {
		return true
	} else if (blog.isFetching) {
		return false
	} else {
		return blog.didInvalidate
	}
}

export function getBlogPostsIfNeeded(blog) {
	return (dispatch) => {
		if (shouldGetBlogPosts(blog)) {
			return dispatch(getBlogPosts(blog))
		}
	}
}

//forum

export function addForumPost(post) {
	return { type: ADD_FORUM_POST, post }
}

export function editForumPost(post) {
	return { type: EDIT_FORUM_POST, post }
}

export function deleteForumPost(post) {
	return { type: DELETE_FORUM_POST, post }
}

function requestForumPosts(forum) {
	return { type: REQUEST_FORUM_POSTS, forum }
}

function receiveForumPosts(forum, json) {
	return {
		type: RECEIVE_FORUM_POSTS,
		forum,
		posts: json.data.childen.map(child => child.data),
		receivedAt: Date.now()
	}
}

function getForumPosts(forum) {
	return dispatch => {
		dispatch(requestForumPosts(forum))
		return fetch('/api/forum/latest')
			.then(response => response.json())
			.then(json => dispatch(recievedForumPosts(forum, json)))
	}
}

function shouldGetForumPosts(forum) {
	if (!forum || forum.length == 0) {
		return true
	} else if (forum.isFetching) {
		return false
	} else {
		return forum.didInvalidate
	}
}

export function getForumPostsIfNeeded(forum) {
	return (dispatch) => {
		if (shouldForumGetPosts(forum)) {
			return dispatch(getForumPosts(forum))
		}
	}
}