const React = require('react')

let error = React.createClass({
	render: function() {
		console.log("error!"+ this.props.error)
		return (
			<section>
				<h1>{this.props.message}</h1>
			</section>
		)
	}
})

module.exports = error