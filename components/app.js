"use strict"

process.env.TZ = 'America/Los_Angeles'

const Express = require('express'),
	https = require('https'),
	app = Express(),
	React = require('react'),
	ReactDOMServer = require('react-dom/server'),
	fs = require('fs'),
	compression = require('compression'),
	router = require('../routes/router'),
	reroutes = require('../routes/reroutes'),
	api = require('./api/routers/main.js'),
	adminapp = require('../admin/router.js'),
	expressJwt = require('express-jwt'),
	config = require('../config')

app.use(compression())

app.engine('jsx',function(filePath,options,callback){
	let basePath = __dirname.split('components'),
		url = filePath.split(basePath[0])[1]
	console.log('engineurl: '+url)
  	let file = require('../'+url),
  		File = React.createFactory(file),
  		component = File(options),
  		output = ReactDOMServer.renderToString(component)
  	return callback(null,output)
})

app.set('views','./components')

app.get('*', (req, res, next) => {

  if (req.url.indexOf("/img/") === 0 || req.url.indexOf("/favicon.ico") === 0) {
    res.setHeader("Cache-Control", "public, max-age=2592000")
    res.setHeader("Expires", new Date(Date.now() + 2592000000).toUTCString())
  }
  next()
})

app.set('view engine','jsx')

app.use(Express.static('./dist'))

const whitelist = [
	'/api/users/login',
	'/api/users/create',
	'/api/demo/activate',
	'/api/demo/verify',
	'/api/blog/latest',
	'/api/blog/all',
	'/api/forum/latest',
	'/api/forum/all',
	'/api/products/category'
]
app.use('/api',expressJwt({secret: config.jwt.secret}).unless({path: whitelist}))

app.use('/api',api)

app.use('/admin',adminapp)

app.use(reroutes)

app.use(router)

const serverOptions = {
  	key: fs.readFileSync('ssl/af-dev.key'),
	cert: fs.readFileSync('ssl/af-dev.crt')
}

const server = https.createServer(serverOptions,app)

server.on('listening', () => {
	console.log('https web app started')	
})

server.listen(443)

module.exports = app