const jQuery = require('jquery')

const handleForm = function(form) {
	jQuery('form#'+form).on('submit', function(e) {
		e.preventDefault()
		let $this = jQuery(this),
			data = {},
			fields = $this.find('input'),
			url = '/api/users/create'
		if (form === 'register') {
			let $password = $this.find('input[name="password"]'),
				$password2 = $this.find('input[name="password2"]')
			if ($password.val() !== $password2.val()) {
				$password2.addClass('error')
				return false
			}
		}
		if (form === 'login') {
			url = '/api/users/login'
		}
		fields  = fields.add($this.find('select'))
		jQuery.each(fields,(i,field) => {
			let $field = jQuery(field),
				key = $field.attr('name'),
				value = $field.val()
			data[key] = value
		})
		jQuery.ajax({
			data: data,
			method: 'POST',
		    dataType: 'json',
			url: url
		})
		.done(function(response) {
			if (form === 'login') {
				jQuery('#close-lightbox').click()
				console.log('login successful ',response)
				return
			}
			$this.find('input').val('')
			$this.find('button').remove()
			$this.find('.form-fields').append('<p>Submission received! Thank you!</p>')
		})
		.fail(function(response) {
			if (form === 'login') {
				console.log('login error ',response)
				$this.find('input[name="password"]').addClass('error')
			}
		})
	})
}

module.exports = handleForm