const React = require('react')

const AboutPage = React.createClass({
	render: function() {
		return (
			<section className="main" id="about">
				<p className="about-text"><strong>Audio Fusion</strong> is an audio technology company founded in 2014. We specialize in bringing the experience of professional audio to new platforms like Virtual Reality and The Web. We are driven to help our community grow as audio professionals and engineers.</p>
				<h3>Meet the Team!</h3>
				<ul className="team">
					<li className="member">
						<div className="team-pic">
							<img src="img/sam.png" alt="Sam Fisher, Founder of Audio Fusion and Creator of Virtual Analog Studio" name="Sam Fisher, Founder of Audio Fusion and Creator of Virtual Analog Studio"/>
						</div>
						<p>
							<strong><i className="fa fa-rebel"></i>Sam Fisher</strong>
							<br />
							<span>Founder</span>
						</p>
					</li>
					<li className="member">
						<div className="team-pic">
							<img src="img/john.png" alt="John Perea, Web Ninja" name="John Perea, Web Ninja"/>
						</div>
						<p>
							<strong><i className="fa fa-empire"></i>John Perea</strong>
							<br />
							<span>Web Ninja</span>
						</p>
					</li>
					<li className="member">
						<div className="team-pic">
							<img src="img/hagai.png"/>
						</div>
						<p>
							<strong><i className="fa fa-bolt"></i>Hagai Davidoff</strong>
							<br />
							<span>Teacher &amp; Tinkerer</span>
						</p>
					</li>
					<li className="member">
						<div className="team-pic">
							<img src="img/umut.png"/>
						</div>
						<p>
							<strong><i className="fa fa-book"></i>Umut Ermec</strong>
							<br />
							<span>Audio Educator &amp; Entrepreneur</span>
						</p>
					</li>
					<li className="member">
						<div className="team-pic">
							<img src="img/veronica.png"/>
						</div>
						<p>
							<strong><img name="Triforce!" src="img/badge_aboutus_veronica.png"/>Veronica Nizama</strong>
							<br />
							<span>Designer, 3D Artist, Hero of Hyrule</span>
						</p>
					</li>
					<li className="member">
						<div className="team-pic">
							<img src="img/dan.png"/>
						</div>
						<p>
							<strong><i className="fa fa-sliders"></i>Dan Blanck</strong>
							<br />
							<span>Audio Engineer, Producer, ..Guru</span>						
						</p>
					</li>
				</ul>
				<h3>Previous Contributors</h3>
				<ul className="team">
					<li className="member">
						<div className="team-pic">
							<img src="img/mitchel.png"/>
						</div>
						<p>
							<strong><i className="fa fa-cube"></i>Mitchel Sanchez</strong>
							<br />
							<span>3D Artist</span>						
						</p>
					</li>
					<li className="member">
						<div className="team-pic">
							<img src="img/tica.png"/>
						</div>
						<p>
							<strong><i className="fa fa-cube"></i>Tica Sansook</strong>
							<br />
							<span>3D Artist</span>						
						</p>
					</li>
					<li className="member">
						<div className="team-pic">
							<img src="img/anthony.png"/>
						</div>
						<p>
							<strong><i className="fa fa-cube"></i>Anthony Davis</strong>
							<br />
							<span>3D Artist</span>						
						</p>
					</li>
				</ul>
			</section>
		)
	}
})

module.exports = AboutPage