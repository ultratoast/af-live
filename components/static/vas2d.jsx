const React = require('react'),
	Lightbox = require('../layouts/lightbox.jsx')

const Vas2D = React.createClass({
	render: function() {
		return (
			<section className="main" id="vas2d">
				<section className="vas-core">
					<ul className="carousel">
						<li className="slide active" style={{backgroundImage: 'url(img/header_vas_1.png)'}}>
							<div className="slide-content">
								<p>Meet VAS, the world's first<br /> Virtual Analog Recording Studio</p>
							</div>
						</li>
					</ul>
					<ul className="core">
						<li className="pitch">
							<div className="circle">
								<img name="Virtual Analog Studio (VAS) is a real home recording studio" alt="Virtual Analog Studio (VAS) is a real home recording studio" src="img/icon_vas_top1.png"/>
							</div>
							<strong>Your Studio, Anywhere</strong>
							<p>Virtual Analog Studio simulates an analog studio environment that could be used anywhere: At home, on the road or 30,000 ft in the air.</p>
						</li>
						<li className="pitch">
							<div className="circle">
								<img name="Virtual Analog Studio (VAS) provides unlimited recording studio access thanks to virtual reality" alt="Virtual Analog Studio (VAS) provides unlimited recording studio access thanks to virtual reality" src="img/icon_vas_top2.png"/>
							</div>
							<strong>On-Demand Studio Time</strong>
							<p>Having a studio with you anywhere means you could now learn, practice and stay fresh on studio workflow on your time and your dime.</p>
						</li>
						<li className="pitch">
							<div className="circle">
								<img name="Experience with Virtual Analog Studio (VAS) translates to professional audio engineering" alt="Experience with Virtual Analog Studio (VAS) translates to professional audio engineering"  src="img/icon_vas_top3.png"/>
							</div>
							<strong>Apply Your Experience</strong>
							<p>Take the skills and techniques developed outside the studio into the studio with confidence. Utilize studio time more effectively.</p>
						</li>
						<li className="pitch">
							<div className="circle">
								<img name="Virtual Analog Studio (VAS) will make you a better audio engineer" alt="Virtual Analog Studio (VAS) will make you a better audio engineer" src="img/icon_vas_top4.png"/>
							</div>
							<strong>A Stronger Engineer</strong>
							<p>You get out of it what you put into it. Embrace all tools to maximize integrity, time &amp; money. What employers/clients love!</p>
						</li>
					</ul>
				</section>
				<section className="vas-main">
					<div className="column">
						<img src="img/icon_vas_logo.png" name="Virtual Analog Studio" alt="Virtual Analog Studio"/>
						<p>
							<strong>We're almost ready!</strong>
							<br/>
							We're building a software simulation of an analog recording studio. It's purpose is to help educate and inspire a whole new generation of audio engineers. VAS is comprised of classic gear emulators and our custom learning modules designed to enhance &amp; maximize the engineer's workflow in real world studios.
							<br />
							A lot of effort and attention to detail is being put into VAS to bring out the best experience upon launch! Stay tuned!!
						</p>
					</div>
					<div className="column">
						<p>
							<strong>Built for Educators, Students, &amp; Graduates</strong>
							<br />
							Virtual Analog Studio (VAS) is the perfect companion in &amp; out of the classroom.
						</p>
						<ul>
							<li><i className="fa fa-chevron-right"></i>Educators! Offer stronger student comprehension through interactivity.</li>
							<li><i className="fa fa-chevron-right"></i>Students! Put classwork to the test at home. Maximize lab-hours in the studio.</li>
							<li><i className="fa fa-chevron-right"></i>Graduates! Stay fresh when looking for work in the industry.</li>
						</ul>
						<br />
						<button className="open-lightbox" data-lightbox="beta">Sign Up for Our Beta</button>
					</div>
				</section>
				<section className="vas-features">
					<div className="column">
						<h3>Features</h3>
						<ul className="feature-list">
							<li className="feature">
								<strong>The Recording Desk</strong>
								<ul>
									<li>Up to 24 channels with in-line processing options.</li>
									<li>Group Busses, Aux Sends, Stereo Returns, Record &amp; Mix Busses.</li>
									<li>Fader Groups</li>
								</ul>
							</li>
							<li className="feature">
								<strong>External Processors</strong>
								<ul>
									<li>Emulations of classic dynamic EQ’s and compressors</li>
									<li>Various time-based effects processing</li>
								</ul>
							</li>
							<li className="feature">
								<strong>Patchbay &amp; More</strong>
								<ul>
									<li>Complex Signal Routing</li>
									<li>Preset Recall</li>
									<li>Internal Audio Player/External Sound-card Input/Rewire Input</li>
									<li>Audio Export Formats (WAV/MP3/AIFF)</li>
								</ul>
							</li>
						</ul>
					</div>
					<div className="column">
						<div className="breakout">
							<strong>Student Design Competition</strong>
							<img src="img/aes_award.png"/>
						</div>
					</div>
				</section>
				<Lightbox />
			</section>
		)
	}
})

module.exports = Vas2D
