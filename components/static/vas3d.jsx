const React = require('react'),
	carousel = require('../layouts/carousel.js')

const Vas3D = React.createClass({
	componentDidMount: function() {
		carousel()
	},
	render: function() {
		return (
			<section className="main" id="vas3d">
				<section className="header3d">
					<h3>Virtual Analog Studio 3D + Virtual Reality</h3>
					<ul className="carousel" id="vas3d-top-carousel">
						<li className="slide active" style={{backgroundImage: 'url(img/vas3d_header_2.jpg)'}}>
							<div className="slide-content">
							</div>
						</li>
					</ul>
					<p>
						We are proud to welcome you into our fully virtualized recording studio
						<br />
						<strong>Studio Time On Your Dime - ANYTIME - ANYWHERE - For ANYONE</strong>
					</p>
				</section>
				<section className="body">
					<div className="column">
						<strong>Our Mission</strong>
						<p>
						To create an AUTHENTIC studio experience. Enable anyone to be dropped instantly into their dream studio
						</p>
						<ul>
							<li>Virtual Gear</li>
							<li>Virtual Band</li>
							<li>Virtual Classic Rooms</li>
							<li>Virtual Studio Inspirations</li>
						</ul>							
					</div>
					<div className="column">
						<img src="/img/vas3d_column_5.jpg" alt="Virtual Analog Studio (VAS) is an authentic home recording studio" name="Virtual Analog Studio (VAS) is an authentic home recording studio"/>
					</div>
					<div className="column desktop">
						<img src="/img/vas3d_column_1.jpg" alt="Experience Legendary Recording Studios in Virtual Reality" name="Experience Legendary Recording Studios in Virtual Reality"/>
					</div>
					<div className="column">
						<strong>Step up to the Plate</strong>
						<br />
						<p>
							Gain HANDS-ON experience with emulated studio technology the same ones used by the pros
						</p>
					</div>
					<div className="column mobile">
						<img src="/img/vas3d_column_1.jpg" alt="Experience Legendary Recording Studios in Virtual Reality" name="Experience Legendary Recording Studios in Virtual Reality"/>
					</div>
					<div className="column">
							<strong>Mobile VR Solution</strong>
							<p>
								Not only do we plan to support a variety of major VR devices (Oculus Rift, HTC VIVE)
								<br />
								We are also supporting SKELETAL TRACKING using an Xbox-Kinect! Users can walk around the studio in virtual reality. You'll need:
							</p>
							<ol>
								<li>A mobile device</li> 
								<li>Mobile-VR headset (Cardboard-Gear)</li>
								<li>Xbox One Kinect</li>
							</ol>
					</div>
					<div className="column">
						<img src="/img/vas3d_column_3.jpg" alt="Virtual Analog Studio supports Xbox Kinect Skeletal Tracking" name="Virtual Analog Studio supports Xbox Kinect Skeletal Tracking"/>
					</div>
					<div className="column desktop">
						<img src="/img/vas3d_column_4.jpg" alt="Virtual Analog Studio allows full custom home recording studio setup" name="Virtual Analog Studio allows full custom home recording studio setup"/>
					</div>
					<div className="column">
						<strong>Customize Your Studio</strong>
						<br />
						<p>
							<em>(Coming Soon)</em>
							<br />
							From the console,external devices, microphones to the acoustic treatment and materials… Your studio will soon be FULLY customizable
						</p>
					</div>
					<div className="column mobile">
						<img src="/img/vas3d_column_4.jpg" alt="Virtual Analog Studio allows full custom home recording studio setup" name="Virtual Analog Studio allows full custom home recording studio setup"/>
					</div>
				</section>
				<section className="gallery">
					<h3>See VAS in action:</h3>
					<ul className="carousel" id="vas3d-image-gallery">
						<li className="slide active" style={{backgroundImage: 'url(img/vas3d_gallery_7.jpg)'}}>
							<div className="slide-content">
							</div>
						</li>
						<li className="slide" style={{backgroundImage: 'url(img/vas3d_gallery_5.jpg)'}}>
							<div className="slide-content">
							</div>
						</li>
					</ul>
					<div data-target="vas3d-image-gallery" className="carousel-nav nav-left" id="prev-arrow-gallery"><i className="fa fa-chevron-left"></i></div>
					<div data-target="vas3d-image-gallery" className="carousel-nav nav-right" id="next-arrow-gallery"><i className="fa fa-chevron-right"></i></div>
				</section>
			</section>
		)
	}
})

module.exports = Vas3D