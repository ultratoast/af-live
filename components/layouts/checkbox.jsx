var React = require('react'),
	jQuery = require('jquery')

var checkbox = function($checkbox) {
	let $input = jQuery('input[name="'+$checkbox.attr('data-input')+'"]')
	if ($checkbox.hasClass('checked')) {
		$checkbox.removeClass('checked')
		$input.val('false')
	} else {
		$checkbox.addClass('checked')
		$input.val('true')
	}
}

const Checkbox = React.createClass({
	componentDidMount: function() {
		jQuery('.checkbox').on('click', function(e) {
			var $this = jQuery(this)
			e.stopPropagation()
			checkbox($this)
		})
	},
	render: function() {
		return (
			<span className="checkbox checked" data-input={this.props.input}></span>
		)
	}
})

module.exports = Checkbox