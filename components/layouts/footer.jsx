const React = require('react'),
	Link = require('react-router/lib/Link')

const googleAnalytics = function(){
	(function(window, document, script, url, r, tag, firstScriptTag) {
	window['GoogleAnalyticsObject']=r;
	window[r] = window[r] || function() {
	  (window[r].q = window[r].q || []).push(arguments)
	};
	window[r].l = 1*new Date();
	tag = document.createElement(script),
	firstScriptTag = document.getElementsByTagName(script)[0];
	tag.async = 1;
	tag.src = url;
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	})(
	window,
	document,
	'script',
	'//www.google-analytics.com/analytics.js',
	'ga'
	);

	var ga = window.ga;

	ga('create', 'UA-76170673-1', 'auto');
  	ga("send", "pageview", window.location);
	return window.ga.apply(window.ga, arguments);
}
const Footer = React.createClass({
	componentDidMount: function() {
		googleAnalytics()
	},
	render: function() {
		return (
			<section className="footer">
				<ul className="nav">
					<li><Link to="/vas2d">VAS 2D</Link></li>
					<li><Link to="/vas3d">VAS 3D</Link></li>
					<li><Link to="/about">About</Link></li>
				</ul>
				<ul className="social">
					<li><a href="https://www.facebook.com/audiofusionpro" target="_blank"><i className="fa fa-facebook-square"></i></a></li>
					<li><a href="https://twitter.com/AudioFusionLLC" target="_blank"><i className="fa fa-twitter-square"></i></a></li>
					<li><a href="https://www.youtube.com/user/AudioFusionPro" target="_blank"><i className="fa fa-youtube-square"></i></a></li>
				</ul>
			</section>
		)
	}
})

module.exports = Footer