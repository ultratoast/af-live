const React = require('react'),
	SiteNav = require('./nav.jsx'),
	Header = require('./header.jsx'),
	Footer = require('./footer.jsx')

const DefaultHeadless = React.createClass({
	render: function() {
		return (
			<div>
				<SiteNav/>
				{this.props.children}
				<Footer/>
			</div>
		)
	}
})

module.exports = DefaultHeadless