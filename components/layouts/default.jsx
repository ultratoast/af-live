const React = require('react'),
	SiteNav = require('./nav.jsx'),
	Header = require('./header.jsx'),
	Footer = require('./footer.jsx')

const Default = React.createClass({
	render: function() {
		return (
			<html>
				<Header/>
				<body>
					<div id="content">
						<div>
							<SiteNav/>
							{this.props.children}
							<Footer/>
						</div>
					</div>
					<script src="/js/client.bundle.js"></script>
				</body>
			</html>
		)
	}
})

module.exports = Default