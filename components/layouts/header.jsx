const React = require('react')

const Header = React.createClass({
	render: function() {
		return (
				<head>
					<title>Audio Fusion</title>
					<meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1" />
					<meta name="keywords" content="Virtual Analog Studio, VAS, Virtual Reality, Audio Fusion, Home Recording Studio, Google Cardboard, Oculus Rift, HTC Vive" />
					<meta name="description" content="Audio Fusion offers a fully virtualized analog recording studio. Virtual Analog Studio 2D and Virtual Analog Studio 3D capture the professional recording studio at home." />
					<link href='https://fonts.googleapis.com/css?family=Nunito:400,700|Audiowide:regular' rel='stylesheet' type='text/css' />
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
					<link rel="stylesheet" href="/css/styles.css" />
				</head>
		)
	}
})

module.exports = Header