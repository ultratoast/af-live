const React = require('react'),
	Link = require('react-router/lib/Link'),
	jQuery = require('jquery'),
	LoginForm = require('./forms/login.jsx')

const SiteNav = React.createClass({
	componentDidMount: function() {
		jQuery(document).ready(function(){
			jQuery('ul.nav').click(function(){
				let $this = jQuery(this)
				if (!$this.is('a')) {
					$this.toggleClass('open')
				}
			}) 	
		})
	},
	render: function() {
		return (
			<section className="header">
				<div className="logo">
					<Link to="/"><h1>Audio Fusion</h1></Link>
				</div>
				<ul className="nav">
					<li><Link to="/vas2d">VAS 2D</Link></li>
					<li><Link to="/vas3d">VAS 3D</Link></li>
					<li><Link to="/about">About</Link></li>
					<li><a href="#" className="open-lightbox" data-lightbox="login">Log In</a></li>
				</ul>
			</section>
		)
	}
})

module.exports = SiteNav