const React = require('react'),
	jQuery = require('jquery'),
	BetaForm = require('./forms/beta.jsx'),
	NewsLetterForm = require('./forms/newsletter.jsx'),
	LoginForm = require('./forms/login.jsx'),
	RegisterForm = require('./forms/register.jsx')

const hideLightbox = function($lightbox,$overlay,$form) {
	$lightbox.removeClass('open')
	$overlay.removeClass('open')
	$form.hide()
}
const lightbox = function($button) {
	var lbtype = $button.attr('data-lightbox'),
		$lightbox = jQuery('.lightbox'),
		$overlay = jQuery('.overlay'),
		$form = jQuery('#'+lbtype)
	$lightbox.find('form').hide()
	$form.show()
	$lightbox.addClass('open')
	$overlay.addClass('open')
	jQuery('#close-lightbox').on('click', function(e) {
		hideLightbox($lightbox,$overlay,$form)
	})
	$overlay.on('click', function(e) {
		hideLightbox($lightbox,$overlay,$form)
	})
}

const Lightbox = React.createClass({
	componentDidMount: function() {
		jQuery('.open-lightbox').on('click', function(e) {
			e.preventDefault()
			var $this = jQuery(this)
			lightbox($this)
		})
	},
	render: function() {
		return (
			<div className="lightbox-wrap">
				<div className="overlay"></div>
				<div className="lightbox">
					<img id="close-lightbox" src="img/button_popup_x.png"/>
					<BetaForm />
					<NewsLetterForm />
					<LoginForm />
					<RegisterForm />
				</div>				
			</div>
		)
	}
})

module.exports = Lightbox