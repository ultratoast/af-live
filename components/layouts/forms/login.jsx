const React = require('react'),
	handleUserForm = require('../../helpers/handleUserForm.js')

const LoginForm = React.createClass({
	componentDidMount: function() {
		handleUserForm('login')
	},
	render: function() {
		return (
			<form id="login">
				<strong className="lightbox-header">Log In</strong>
				<section className="form-fields">
					<label htmlFor="email">Email</label>
					<br/>
					<input name="email" type="email"/>
					<br />
					<label htmlFor="password">Password</label>
					<br />
					<input name="password" type="password"/>
					<br />
					<button>Log In</button>
					<br/>
					<span className="register-link">Don't have an Account? <a href="#" className="open-lightbox" data-lightbox="register">Register Now</a></span>
				</section>
			</form>
		)
	}
})

module.exports = LoginForm