const React = require('react'),
	handleUserForm = require('../../helpers/handleUserForm')

const NewsLetterForm = React.createClass({
	componentDidMount: function() {
		handleUserForm('newsletter')
	},
	render: function() {
		return (
			<form id="newsletter">
				<strong className="lightbox-header">Newsletter signup</strong>
				<section className="form-fields">
					<label htmlFor="email">Email</label>
					<br />
					<input name="email" type="email" />
					<br />
					<label htmlFor="company">Company</label>
					<br />
					<input name="company" type="company" />
					<br />
					<input name="registeredFrom" type="hidden" value="newsletter"/>
					<br />
					<input name="sendNewsletter" type="hidden" value="true"/>
					<br />
					<button type="submit">Submit</button>
				</section>
			</form>
			)
	}
})

module.exports = NewsLetterForm