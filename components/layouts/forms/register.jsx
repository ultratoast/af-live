const React = require('react'),
	handleUserForm = require('../../helpers/handleUserForm'),
	Checkbox = require('../checkbox.jsx')

const RegisterForm = React.createClass({
	componentDidMount: function() {
		handleUserForm('register')
	},
	render: function() {
		return (
			<form id="register">
				<strong className="lightbox-header">Beta Signup</strong>
				<section className="form-fields">
					<label htmlFor="email">Email*</label>
					<br />
					<input name="email" type="email" required/>
					<br />
					<label htmlFor="firstname" required>First Name*</label>
					<br />
					<input name="firstname" type="text" />
					<br />
					<label htmlFor="lastname" required>Last Name*</label>
					<br />
					<input name="lastname" type="text" />
					<br />
					<label htmlFor="password" required>Password*</label>
					<br />
					<input name="password" type="password" />
					<br />
					<label htmlFor="password2" required>Confirm Password*</label>
					<br />
					<input name="password2" type="password" />
					<br />
					<label htmlFor="experience">Experience Level</label>
					<br />
					<select name="experience">
						<option value="student">Student</option>
						<option value="educator">Educator</option>
						<option value="professional">Professional</option>
						<option value="noob">Noob</option>
					</select>
					<br />
					<input name="registeredFrom" type="hidden" value="normal"/>
					<br />
					<button type="submit">Submit</button>
				</section>
			</form>
		)
	}
})

module.exports = RegisterForm