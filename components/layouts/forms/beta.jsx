const React = require('react'),
	handleUserForm = require('../../helpers/handleUserForm'),
	Checkbox = require('../checkbox.jsx')

const BetaForm = React.createClass({
	componentDidMount: function() {
		handleUserForm('beta')
	},
	render: function() {
		return (
			<form id="beta">
				<strong className="lightbox-header">Beta Signup</strong>
				<section className="form-fields">
					<label htmlFor="email">Email</label>
					<br />
					<input name="email" type="email" />
					<br />
					<label htmlFor="firstname">First Name</label>
					<br />
					<input name="firstname" type="text" />
					<br />
					<label htmlFor="lastname">Last Name</label>
					<br />
					<input name="lastname" type="text" />
					<br />
					<label htmlFor="experience">Experience Level</label>
					<br />
					<select name="experience">
						<option value="student">Student</option>
						<option value="educator">Educator</option>
						<option value="professional">Professional</option>
						<option value="noob">Noob</option>
					</select>
					<br />
					<input name="registeredFrom" type="hidden" value="beta"/>
					<br />
					<input name="sendNewsletter" type="hidden" value="true"/>
					<Checkbox input="sendNewsletter"/>
					<label htmlFor="sendNewsletter">I'd like to receive newsletters</label>
					<br />
					<button type="submit">Submit</button>
				</section>
			</form>
			)
	}
})

module.exports = BetaForm