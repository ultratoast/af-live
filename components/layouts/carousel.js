const jQuery = require('jquery')

var carousel = function() {
	var nav = jQuery('.carousel-nav')
	nav.each(function(i,link){
		jQuery(link).click(function(){
			var $this = jQuery(this),
				target = $this.attr('data-target'),
				slides = jQuery('#'+target+' .slide'),
				$active = jQuery('#'+target+' .active'),
				$prev = $active.prev('.slide').length > 0 ? $active.prev('.slide') : jQuery(slides[slides.length - 1]),
				$next = $active.next('.slide').length > 0 ? $active.next('.slide') : jQuery(slides[0])
			$active.removeClass('active')
			if ($this.hasClass('nav-left')) {
				$prev.addClass('active')
			}
			if ($this.hasClass('nav-right')) {
				$next.addClass('active')
			}
			window.clearInterval(timeout)
			timeout = window.setInterval(function(){
				jQuery('.carousel-nav.nav-right').each(function(){
					jQuery(this).click()
				})
			}, 5000)
		})
	})
	var timeout = window.setInterval(function(){
		jQuery('.carousel-nav.nav-right').each(function(){
			jQuery(this).click()
		})
	}, 5000)
}

module.exports = carousel