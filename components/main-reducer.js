import { combineReducers  } from 'redux'

const {blog,forum} = require('./reducers.js').default

const mainReducer = combineReducers({
	blog
})

export default mainReducer