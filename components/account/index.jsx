const React = require('react')

const AccountPage = React.createClass({
	render: function() {
		return (
			<section className="main" id="account">
				<div className="column">
					<strong>Basic Info</strong>
					<ul>
						<li>First Name</li>
						<li>Last Name</li>
						<li>Email</li>
						<li>Company</li>
						<li>Send Newsletter?</li>
						<li><a href="#" className="open-lightbox" data-lightbox="password">Change Password</a></li>
					</ul>
				</div>
				<div className="column">
					<strong>Product Licenses</strong>
					<ul>
						<li>VAS2D <span className="status activate">Activate</span></li>
						<li>VAS3D <span className="status active">Active</span></li>
					</ul>
				</div>
				<div className="column">
					<strong>Forum Threads</strong>
					<ul>
						<li><a href="/forum">Post Title</a></li>
					</ul>
				</div>
				<div className="column">
					<strong>Shopping Cart</strong>
					<ul>
						<li>VAS 2D Plugin <span className="status remove">X</span></li>
					</ul>
				</div>
			</section>
		)
	}
})

module.exports = AccountPage