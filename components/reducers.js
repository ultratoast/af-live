import { combineReducers } from 'redux'
import {
	REQUEST_BLOG_POSTS,RECEIVE_BLOG_POSTS,REQUEST_FORUM_POSTS,RECEIVE_FORUM_POSTS
} from './actions'

const initialState = []

function getblog(state = {
	isFetching: false,
	didInvalidate: false,
	items: []
}, action) {
	let actionObject = {
		REQUEST_BLOG_POSTS: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		RECEIVE_BLOG_POSTS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			items: action.posts,
			lastUpdated: action.receivedAt
		}),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']
}

function handleActions(state, action) {
	return Object.assign({}, state, {
		posts: getblog(state.blog, action)
	})
}
function blog(state, action) {
	if (typeof state === "undefined") {
		return initialState
	}
	let actionObject = {
		REQUEST_BLOG_POSTS: handleActions(state, action),
		RECEIVE_BLOG_POSTS: handleActions(state, action),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']	
}

function getforum(state = {
	isFetching: false,
	didInvalidate: false,
	items: []
}, action) {
	let actionObject = {
		REQUEST_FORUM_POSTS: Object.assign({}, state, {
			isFetching: true,
			didInvalidate: false
		}),
		RECEIVE_FORUM_POSTS: Object.assign({}, state, {
			isFetching: false,
			didInvalidate: false,
			items: action.posts,
			lastUpdated: action.receivedAt
		}),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']
}

function handleActions(state, action) {
	return Object.assign({}, state, {
		posts: getforum(state.forum, action)
	})
}
function forum(state, action) {
	if (typeof state === "undefined") {
		return initialState
	}
	let actionObject = {
		REQUEST_FORUM_POSTS: handleActions(state, action),
		RECEIVE_FORUM_POSTS: handleActions(state, action),
		'default': state
	}
	return actionObject[action.type] || actionObject['default']	
}

export default {blog,forum}