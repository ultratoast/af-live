var React = require('react'),
	Post = require('./post.jsx'),
	// actions = require('../actions')

var BlogIndex = React.createClass({
	componentDidMount: function() {
		// let { dispatch, blog } = this.props
		// dispatch(actions.getPostsIfNeeded(blog))	
	},
	getInitialState: function () {
	  	// return {blog: this.props.blog}
	},
	render: function(){
		let statejson = JSON.stringify({blog:this.props.blog})
		let blog = this.props.blog || '<li>No Data</li>'
			blog = blog.map(function(post,i){
				let isPublic = post.isPublic ? <Post key={i} post={post}/> : null
				return (
					isPublic
				)
			}.bind(this))
			blog = blog.filter(function(post){
				if (post) {
					return true
				} else {
					return false
				}
			})
			return (
				<div>
					<ul className="blog">
						{blog}
					</ul>
					<script id="state" data-state={statejson}></script>
				</div>
			)
			
	}
})

module.exports = BlogIndex