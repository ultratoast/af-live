"use strict"

const Express = require('express'),
	React = require('react'),
	ReactDOMServer = require('react-dom/server'),
	RouterContext = require('react-router/lib/RouterContext'),
	Router = Express.Router(),
	router = require('react-router').Router,
	Provider = require('react-redux/lib/components/Provider'),
	createStore = require('redux/lib/createStore').default,
	applyMiddleware = require('redux/lib/applyMiddleware').default,
	thunk = require('redux-thunk').default,
	match = require('react-router/lib/match'),
	routes = require('./routes'),
	mainReducer = require('../components/main-reducer').default    				

// console.log('initialState ',initialState)

Router.route('*')
	.get(function(req,res,next){
		// console.log('routeurl '+req.url)
	 	match({ routes:routes, location: req.url }, (error, redirectLocation, renderProps) => {
		    if (error) {
		    	//return the error view
		      	res.render('./misc/error',{message:error.message,error:error})
		    } else if (redirectLocation) {
		    	//follow redirect
		      	res.redirect(302, redirectLocation.pathname + redirectLocation.search)
		    } else if (renderProps) {
		    	//render redux + react router
      			let store = applyMiddleware(thunk)(createStore)(mainReducer),
				    initialState = store.getState(), 
				    provider = React.createFactory(Provider.default),
      				routerContext = React.createFactory(RouterContext)
				let component = provider({store: store},
					routerContext(renderProps)
				)
				res.send(ReactDOMServer.renderToStaticMarkup(component))
		      	// res.send(ReactDOMServer.renderToString(component))
		    } else {
		      	res.send('Not found')
		    }
		    // next()
	  	})
	  	// next()
	})

module.exports = Router