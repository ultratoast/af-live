"use strict"
var isBrowser=new Function("try {return this===window;}catch(e){ return false;}")
if (!isBrowser()) {
	require('babel-register')({
	  presets: ['es2015','react']
	}) //fix es6 + jsx templates for require
}

const Default = require('../components/layouts/default.jsx'),
	DefaultHeadless = require('../components/layouts/defaultheadless.jsx'),
	HomeIndex = require('../components/home/index.jsx'),
	// BlogIndex = require('../components/blog/index.jsx'),
	Vas2D = require('../components/static/vas2d.jsx'),
	Vas3D = require('../components/static/vas3d.jsx'),
	AboutPage = require('../components/static/about.jsx'),
	ErrorPage = require('../components/misc/error.jsx')
	// BlogContainer = require('../components/blog/getblog').default

const RouteConfig = [{
	path: '/',
	indexRoute: {component: HomeIndex},
	component: DefaultHeadless,
	// indexRoute: { onEnter: (nextState, replace) => replace('/home') },
	childRoutes: [
		{path: 'home', component:HomeIndex},
		{path: 'vas2d', component: Vas2D},
		{path: 'vas3d', component: Vas3D},
		{path: 'about', component: AboutPage},
		{
			path: 'misc',
			childRoutes: [
				{path: 'error', component:ErrorPage}
			]
		}
	]
}]

if (!isBrowser()) {
	RouteConfig[0].component = Default
}

module.exports = RouteConfig