"use strict"

const Express = require('express'),
	Router = Express.Router()

Router.get('/virtual-analog-studio', (req,res) => {
	res.redirect('/vas3d')
})

module.exports = Router