const config = {
	"env": "dev",
	"host": "127.0.0.1",
	"jwt": {
		"secret": "Anderso0n.Pa@k"
	},
	"access_levels": {
		"admin": "admin",
		"user":"user",
		"anon":"anon"
	}
}

module.exports = config